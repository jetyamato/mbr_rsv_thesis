# MBR Reservation System (for thesis) #

## What is this repository for? ##
This repository is for the development of the said system above.

## Who can collaborate? ##
Any PHP programmer with a background with CodeIgniter or MVC Architecture can collaborate to this project.

## How do I get set up? ##

### Software requirements for project development: ###

* XAMPP for Windows
* CodeIgniter Framework
* Twitter Bootstrap
* JQuery library
* JQuery Datepair library (https://github.com/jonthornton/Datepair.js)

### Configuration ###

* Install XAMPP. After installation, go to the 'xampp' installation directory.
* Create 'redesign' directory in 'xampp/htdocs'.
* Go to redesign directory and extract CodeIgniter Framework, leaving you with a 'CodeIgniter-x.y.z' directory. Move the contents of the CodeIgniter directory into the parent directory. Delete the empty CodeIgniter directory.
* Clone this repository, leaving you with a 'mbr_rsv_thesis' directory. Go to that directory and you will see directories corresponding to CodeIgniter's file structure. Copy (and overwrite) the contents of each directory of the cloned repository into the corresponding CodeIgniter directory.

### *Dependencies* ###

### Database configuration ###

* After cloning the repository, you will see a SQL file inside.
* Open MySQL Query Browser (or any other tool) and create a database named 'reservation'.
* Open the SQL file and execute the script.

### How to run tests ###

## Contribution guidelines ##

* *Writing tests*
* *Code review*
* *Other guidelines*

## Who do I talk to? ##
If you are interested to devote your time contributing or collaborating to this project you may contact me at tamayo209@gmail.com.