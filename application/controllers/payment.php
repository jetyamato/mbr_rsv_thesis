<?php

class Payment extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('url','cookie','form'));
        $this->load->library(array('form_validation','input','paypalrest'));
        $this->load->model(array('CustomerModel','PaymentModel','CmsModel','ReserveModel'));
        session_start();
	}

	public function index() {
        $roomlist = null;
		$c = get_cookie('customer');
        if ($c == NULL || $c == false) {
            $data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>You must login to access this page.</div>');
            $data2 = array('part' => 'Login');
            $_SESSION['access'] = base_url() . 'payment';
            $this->load->view('login-reg/loginheader', $data2);
            $this->load->view('login-reg/loginview', $data);
            $this->load->view('footer');
            return false;
        }

        $name = $this->CustomerModel->getCustomer($c, false);
        if ($name == false) {
            $data = array(
                'error'       => 'No user data. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        $data = array(
            'cust'      => '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Hello, ' . $name['firstname'] . '!<b class="caret"></b></strong></a><ul class="dropdown-menu"><li><a href=""'. base_url() . 'payment">View Reservations</a></li></ul></li>',
            'login'     => '<a href="' . site_url() . 'logout" class="navbar-link">Logout</a>',
            'alerts'    => ''
        );
        $this->load->view('payment/payheader', $data);

        $rsv = $this->PaymentModel->getRsvDetails($c);
        if ($rsv == false):
            $_SESSION['alerts'] = '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>You have no pending or active reservations.</div>';
            redirect(base_url());
            return false;
        else:
        	$data = array(
        		'rsv'		=> $rsv
        	);
        	$this->load->view('payment/paymentview', $data);

            foreach ($rsv as $r):
                $costs = $this->PaymentModel->getBreakdown($r['rsv_id']);
                if ($costs == false) {
                    $data = array(
                        'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                    );
                    $this->load->view('error', $data);
                    return false;
                }

                $rsvroom = $this->PaymentModel->getRsvRooms($r['rsv_id']);
                if ($rsvroom == false) {
                    $data = array(
                        'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                    );
                    $this->load->view('error', $data);
                    return false;
                }

                $w = 0;
                foreach ($rsvroom as $rsvr):
                    $room = $this->ReserveModel->getRoomsRefined((int)$rsvr['room_id']);
                    if ($room == false) {
                        $data = array(
                            'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                        );
                        $this->load->view('error', $data);
                        return false;
                    }

                    $content = $this->CmsModel->getContent((int)$rsvr['room_id']);
                    if ($content == false) {
                        $data = array(
                            'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                        );
                        $this->load->view('error', $data);
                        return false;
                    }

                    $data = array(
                        'room'  => $room,
                        'c'     => $content,
                        'w'     => $w
                    );

                    $roomlist .= $this->load->view('payment/roomslist', $data, true);
                    $w++;
                endforeach;

                $data = array(
                    'details'   => $r,
                    'roomlist'  => $roomlist,
                    'costs'     => $costs
                );
                $this->load->view('payment/rsvlist', $data);

                $roomlist = null;
            endforeach;
        	$this->load->view('payment/payfooter');
        endif;
	}

    public function payDown($rsvid) {
        $c = get_cookie('customer');
        if ($c == NULL || $c == false) {
            $data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>You must login to access this page.</div>');
            $data2 = array('part' => 'Login');
            $_SESSION['access'] = base_url() . 'payment';
            $this->load->view('login-reg/loginheader', $data2);
            $this->load->view('login-reg/loginview', $data);
            $this->load->view('footer');
            return false;
        }
        
        $row = $this->PaymentModel->getRsv($rsvid);

        if ($row == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }

        switch ($row[0]['pay_method']):
            case 'Online':
            case 'Credit':
                try {
                    $this->PaymentModel->createTrans($rsvid, ($row[0]['grand_total'] * 0.30), 'Online');
                    $payment = $this->paypalrest->makePaymentUsingPayPal(($row[0]['grand_total'] * 0.30), 'PHP', 'Downpayment', '30% Amount of Grand Total', 'Confirmation of Reservation', base_url() . 'paysuccess', base_url() . 'paycancel');
                    $this->PaymentModel->updateTrans($rsvid, $payment->getId(), 'Downpayment');
                    header("Location: " . $this->paypalrest->getLink($payment->getLinks(), "approval_url") );
                    //$payment = $this->paypalrest->executePayment($order['payment_id'], $_GET['PayerID']);
                } catch (PPConnectionException $ex) {
                    $message = parseApiError($ex->getData());
                    $messageType = "error";
                } catch (Exception $ex) {
                    $message = $ex->getMessage();
                    $messageType = "error";
                }
                break;
            case 'Money Transfer':
                $this->PaymentModel->createTrans($rsvid, ($row[0]['grand_total'] * 0.30), 'Money Transfer');
                redirect(base_url() . 'paysuccess');
                break;
        endswitch;
    }
}