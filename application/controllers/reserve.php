<?php

class Reserve extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url','cookie'));
        $this->load->library(array('form_validation','input'));
        $this->load->model(array('LoginModel','ReserveModel','CustomerModel','CmsModel'));
        session_start();
    }

    public function index() {
        $curtime = mktime();

        if (!($curtime >= strtotime('7:00:00') && $curtime <= strtotime('21:00:00'))):
            $_SESSION['alerts'] = '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Reservation is only available during 7:00 AM to 9:00 PM.</div>';
            redirect(base_url());
        endif;

        if (!isset($_SESSION['rsvdata'])) {
            $_SESSION['rsvdata'] = array();
        }

        if (!isset($_SESSION['warn'])) {
            $_SESSION['warn'] = -1;
        }
        else if (isset($_SESSION['warn'])) {
            $_SESSION['warn'] = -1;
        }

        $c = get_cookie('customer');
        if ($c == NULL || $c == false) {
            $data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>You must login to access this page.</div>');
            $data2 = array('part' => 'Login');
            unset($_SESSION['rsvdata']);
            unset($_SESSION['warn']);
            $_SESSION['access'] = base_url() . 'reserve';
            $this->load->view('login-reg/loginheader', $data2);
            $this->load->view('login-reg/loginview', $data);
            $this->load->view('footer');
            return false;
        }

        $name = $this->CustomerModel->getCustomer($c, false);
        if ($name == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        $data = array(
            'cust'      => '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Hello, ' . $name['firstname'] . '!<b class="caret"></b></strong></a><ul class="dropdown-menu"><li><a href="'. base_url() . 'payment">View Reservations</a></li></ul></li>',
            'login'     => '<a href="' . site_url() . 'logout" class="navbar-link">Logout</a>',
            'alerts'    => ''
        );
        $this->load->view('core/cleanheader', $data);
        
        $methods = $this->ReserveModel->getPaymentMethods();
        if ($methods == FALSE || $methods == 0) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data = array('methods' => $methods);
        }

        $r = $this->ReserveModel->getRoomsRefined();

        if ($r == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data['room'] = $r;
        }

        $roomtype = $this->ReserveModel->getRoomType();

        if ($roomtype == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data['rtype'] = $roomtype;
        }

        $this->load->view('reserve/reserveview', $data);

        if (count($_SESSION['rsvdata']) > 0):
            $r = $this->ReserveModel->getRoomsRefined($_SESSION['rsvdata'], null, true);
        else:
            $r = $this->ReserveModel->getRoomsRefined();
        endif;
        
        if ($r == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data = array(
                'room' => $r
            );
        }

        $w = 0;
        foreach ($r as $row) {
            $content = $this->CmsModel->getContent($row['room_id']);
            if ($content == false) {
                $data2 = array(
                    'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                );
                $this->load->view('error', $data2);
                return false;
            }
            else {
                $data['c'] = $content[0];
            }

            $data['room'] = $row;
            $data['w'] = $w;
            if (count($_SESSION['rsvdata']) > 0):
                if (!array_search($row['room_id'], $_SESSION['rsvdata'])):
                    $this->load->view('reserve/roomslist', $data);
                endif;
            else:
                $this->load->view('reserve/roomslist', $data);
            endif;
            $w++;

        }
        $this->load->view('reserve/rsvfooter');
        $this->load->view('footer');
    }

    public function createReserve($form = null, $roomid = null) {
        $c = get_cookie('customer');
        if ($c == NULL || $c == false) {
            $data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>You must login to access this page.</div>');
            $data2 = array('part' => 'Login');
            unset($_SESSION['rsvdata']);
            unset($_SESSION['warn']);
            $_SESSION['access'] = base_url() . 'reserve';
            $this->load->view('login-reg/loginheader', $data2);
            $this->load->view('login-reg/loginview', $data);
            $this->load->view('footer');
            return false;
        }

        $checkin = null;
        $sched = null;
        $remarks = null;
        $alerts = null;
        $errors = array();
        $config = array(
            array(
                'field'     => 'numberofadults',
                'label'     => 'Number of Adults',
                'rules'     => 'required|is_natural_no_zero'
            ),
            array(
                'field'     => 'numberofkids',
                'label'     => 'Number of Kids',
                'rules'     => 'required'
            ),
            array(
                'field'     => 'datecheckout',
                'label'     => 'Date of Check-out',
                'rules'     => 'required'
            )
        );

        if ($form != null && $roomid != null):
            switch ($form):
                case 'add':
                    $_SESSION['rsvdata'][] = $roomid;
                    break;
                
                case 'remove':
                    unset($_SESSION['rsvdata'][array_search($roomid, $_SESSION['rsvdata'])]);
                    break;
            endswitch;
        elseif ($form == null):
            $this->form_validation->set_rules($config);
            $errors = array();
            $paymentmethod = null;
            if ($this->form_validation->run() === false):
                $errors[] = validation_errors();
            endif;
            if ($this->input->post('sched') == null && $this->input->post('rbtonsite') != 'walkin'):
                $errors[] = '<p>You should pick a Swimming Schedule.</p>';
            endif;
            if ($this->input->post('numberofadults') == 'none'):
                $errors[] = '<p>You should pick the Number of Adults.</p>';
            endif;
            if ($this->input->post('rbtonsite') == null && $this->input->post('datecheckin') == null):
                $errors[] = '<p>You should pick the Check-in Date.</p>';
            endif;
            if ($this->input->post('datecheckout') == 'none'):
                $errors[] = '<p>You should pick the Number of Nights.</p>';
            endif;
            if ($this->input->post('rbtonsite') == null && $this->input->post('paymentmethod') == 'none'):
                $errors[] = '<p>You should pick the Payment Method.</p>';
            endif;
            if ($this->ReserveModel->checkValidDate($this->input->post('datecheckin')) == false):
                $errors[] = '<p>Room is reserved for another customer in the check-in date you provided.</p>';
            endif;

            if (count($errors) == 0):
                if ($_SESSION['warn'] == -1):
                    $_SESSION['warn'] = 0;
                elseif ($_SESSION['warn'] == 0):
                    $_SESSION['warn'] = 1;
                endif;

                if ($_SESSION['warn'] == 0):
                    $alerts = $this->checkCapacity();
                elseif ($_SESSION['warn'] == 1):
                    unset($_SESSION['warn']);
                endif;
                
                if ($alerts == null):
                    if ($this->input->post('rbtonsite') == null):
                        switch ($this->input->post('paymentmethod')) {
                            case 1:
                                $paymentmethod = 'Online';
                                break;
                            case 2:
                                $paymentmethod = 'Credit';
                                break;
                            
                            case 3:
                                $paymentmethod = 'Money Transfer';
                                break;
                        };
                        $sched = $this->input->post('sched');
                        $checkin = date('Y-m-d', strtotime($this->input->post('datecheckin')));
                    elseif ($this->input->post('rbtonsite') != null):
                        if (strtotime(mktime()) >= strtotime('08:00:00') && strtotime(mktime()) < strtotime('17:00:00')):
                            $sched = 'day';
                        elseif (strtotime(mktime()) >= strtotime('17:00:00') && strtotime(mktime()) < strtotime('02:00:00')):
                            $sched = 'night';
                        endif;

                        $checkin = date('Y-m-d', mktime());
                        $paymentmethod = 'On Site';
                        $remarks = 'Walk-in transaction';
                    endif;

                    $d = $this->input->post('datecheckout') * 86400;
                    
                    if ($this->input->post('rbtonsite') == null):
                        $checkout = date('Y-m-d', strtotime($this->input->post('datecheckin')) + $d);
                    else:
                        $checkout = date('Y-m-d', mktime() + $d);
                    endif;

                    if ($this->input->post('numberofkids') == 'none'):
                        $kids = 0;
                    else:
                        $kids = $this->input->post('numberofkids');
                    endif;
                    $this->ReserveModel->createRsv($c, $this->input->post('numberofadults'), $kids, $this->input->post('sched'), $checkin, $checkout, date('Y-m-d', mktime()), $_SESSION['rsvdata'], $paymentmethod, time(), $remarks);
                    unset($_SESSION['rsvdata']);
                    redirect(base_url());
                    else:
                        unset($_SESSION['warn']);
                    endif;    
                endif;
        endif;

        $name = $this->CustomerModel->getCustomer($c, false);
        if ($name == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }

        $data = array(
            'cust'      => '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Hello, ' . $name['firstname'] . '!<b class="caret"></b></strong></a><ul class="dropdown-menu"><li><a href="'. base_url() . 'payment">View Reservations</a></li></ul></li>',
            'login'     => '<a href="' . site_url() . 'logout" class="navbar-link">Logout</a>'
        );
        $this->load->view('core/cleanheader', $data);

        $methods = $this->ReserveModel->getPaymentMethods();
        if ($methods == FALSE || $methods == 0) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data = array('methods' => $methods);
        }
        
        $r = $this->ReserveModel->getRoomsRefined();

        if ($r == false) {
            $data = array(
                'error'       => 'Database entry not retrieved.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data['room'] = $r;
        }

        $roomtype = $this->ReserveModel->getRoomType();

        if ($roomtype == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data['rtype'] = $roomtype;
        }

        if (count($errors) > 0):
            $data['alerts'] = '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' . implode($errors) . '</div>';
            $this->load->view('reserve/reserveview', $data);
        elseif ($alerts != null):
            $data['alerts'] = '<div class="alert alert-warning"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' . $alerts . '</div>';
            $this->load->view('reserve/reserveview', $data);
        else:
            $data['alerts'] = '';
            $this->load->view('reserve/reserveview', $data);
        endif;

        if (count($_SESSION['rsvdata']) > 0):
            $r = $this->ReserveModel->getRoomsRefined($_SESSION['rsvdata'], null, true);
        else:
            $r = $this->ReserveModel->getRoomsRefined();
        endif;
        
        if ($r == false) {
            $data = array(
                'error'       => 'Database entry not retrieved.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data = array(
                'room' => $r
            );
        }

        $w = 0;
        foreach ($r as $row) {
            $content = $this->CmsModel->getContent($row['room_id']);
            if ($content == false) {
                $data2 = array(
                    'error'        => 'Content not retrieved.'
                );
                $this->load->view('error', $data2);
                return false;
            }
            else {
                $data['c'] = $content[0];
            }

            $data['room'] = $row;
            $data['w'] = $w;
            if (count($_SESSION['rsvdata']) > 0):
                if (!array_search($row['room_id'], $_SESSION['rsvdata'])):
                    $this->load->view('reserve/roomslist', $data);
                endif;
            else:
                $this->load->view('reserve/roomslist', $data);
            endif;
            $w++;

        }
        $this->load->view('reserve/rsvfooter');
        $this->load->view('footer');

    }

    private function checkCapacity() {
        $lowerLim = 0;
        $upperLim = 0;
        $totalGuests = $this->input->post('numberofadults') + $this->input->post('numberofkids');
        $row = null;

        foreach ($_SESSION['rsvdata'] as $r) {
            $room = $this->ReserveModel->getRoom($r);

            if ($room == false):
                $data = array(
                    'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                );
                $this->load->view('error', $data);
                return false;
            endif;

            switch ($room[0]['room_capacity_desc']) {
                case '2persons':
                    $row = $this->ReserveModel->getCapacity('2persons');
                    break;
                case '6persons':
                    $row = $this->ReserveModel->getCapacity('6persons');
                    break;
                case '10plus':
                    $row = $this->ReserveModel->getCapacity('10plus');
                    break;
                case '10persons':
                    $row = $this->ReserveModel->getCapacity('10persons');
                    break;
            };
            $x = $row[0]['min_capacity'];
            $y = $row[0]['max_capacity'];
            $lowerLim = $x;
            $upperLim = $y;
        }

        if (!($totalGuests >= $lowerLim && $totalGuests <= $upperLim)):
            return '<p><strong>Reminder:</strong> The rooms you have chosen <strong>might not have the right capacity</strong> for you. You may <strong>click the Proceed button</strong> to proceed, or <strong>click the Cancel button</strong> to edit your details.</p><input type="button" name="btnok" id="btnok" class="btn btn-success btn-xs" value="Proceed" /> <input type="button" name="btnreload" id="btnreload" class="btn btn-primary btn-xs" value="Cancel" />';
        else:
            return '';
        endif;
    }

    public function filterRooms() {
        $c = get_cookie('customer');
        if ($c == NULL || $c == false):
            $data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>You must login to access this page.</div>');
            $data2 = array('part' => 'Login');
            unset($_SESSION['rsvdata']);
            unset($_SESSION['warn']);
            $_SESSION['access'] = base_url() . 'reserve';
            $this->load->view('login-reg/loginheader', $data2);
            $this->load->view('login-reg/loginview', $data);
            $this->load->view('footer');
            return false;
        endif;

        $name = $this->CustomerModel->getCustomer($c, false);
        if ($name == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        $data = array(
            'cust'      => '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Hello, ' . $name['firstname'] . '!<b class="caret"></b></strong></a><ul class="dropdown-menu"><li><a href="'. base_url() . 'payment">View Reservations</a></li></ul></li>',
            'login'     => '<a href="' . site_url() . 'logout" class="navbar-link">Logout</a>',
            'alerts'    => ''
        );
        $this->load->view('core/cleanheader', $data);
        
        $methods = $this->ReserveModel->getPaymentMethods();
        if ($methods == FALSE || $methods == 0) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data = array('methods' => $methods);
        }

        $r = $this->ReserveModel->getRoomsRefined();

        if ($r == false) {
            $data = array(
                'error'       => 'Database entry not retrieved.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data['room'] = $r;
        }

        $roomtype = $this->ReserveModel->getRoomType();

        if ($roomtype == false) {
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data['rtype'] = $roomtype;
        }

        $this->load->view('reserve/reserveview', $data);

        switch ($this->input->post('filtertype')):
            case 'all':
                if (count($_SESSION['rsvdata']) > 0):
                    $r = $this->ReserveModel->getRoomsRefined($_SESSION['rsvdata'], true);
                else:
                    $r = $this->ReserveModel->getRoomsRefined();
                endif;
                break;
            
            default:
                if (count($_SESSION['rsvdata']) > 0):
                    $r = $this->ReserveModel->getRoomsRefined($_SESSION['rsvdata'], $this->input->post('filtertype'), true);
                else:
                    $r = $this->ReserveModel->getRoomsRefined($this->input->post('filtertype'));
                endif;
                break;
        endswitch;

        if ($r == false) {
            $data = array(
                'error'       => 'Database entry not retrieved.'
            );
            $this->load->view('error', $data);
            return false;
        }
        else {
            $data = array(
                'room' => $r
            );
        }

        $w = 0;
        foreach ($r as $row) {
            $content = $this->CmsModel->getContent($row['room_id']);
            if ($content == false) {
                $data2 = array(
                    'error'        => 'Content not retrieved.'
                );
                $this->load->view('error', $data2);
                return false;
            }
            else {
                $data['c'] = $content[0];
            }

            $data['room'] = $row;
            $data['w'] = $w;
            if (count($_SESSION['rsvdata']) > 0):
                if (!array_search($row['room_id'], $_SESSION['rsvdata'])):
                    $this->load->view('reserve/roomslist', $data);
                endif;
            else:
                $this->load->view('reserve/roomslist', $data);
            endif;
            $w++;

        }
        $this->load->view('reserve/rsvfooter');
        $this->load->view('footer');
    }
}