<?php

class Verify extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('CustomerModel');
		$this->load->library('parser');
		$this->load->helper('url');
	}

	public function index() {
		$this->load->view('login-reg/versuccess');
	}

	public function verifySalt($salt) {
		$cdata = $this->CustomerModel->getCustomer(null, true, $salt);

		if ($cdata == false) {
			$data = array('error' => 'Invalid user to be verified.');

			$this->parser->parse('error', $data);
		}
		else if ($cdata['cust_status'] == 'confirmed') {
			$data = array('error' => 'Invalid user to be verified.');

			$this->parser->parse('error', $data);
		}
		else {
			$this->CustomerModel->setCustomer($cdata['user_id']);
			redirect(site_url() . 'verify');
		}
	}
}