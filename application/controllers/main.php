<?php

class Main extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('cookie','url'));
		$this->load->model('CustomerModel');
		session_start();
		if (isset($_SESSION['warn'])) {
			unset($_SESSION['warn']);
		}
	}

	public function index() {
		$c = get_cookie('customer');
		if ($c == null) {
			$data = array(
				'cust' => '<li><p class="navbar-text"><strong>Welcome, Guest!</strong></p><li>',
				'login' => '<a href="'.base_url().'login" class="navbar-link">Login</a>'
			);
		}
		else if ($c != null) {
			$name = $this->CustomerModel->getCustomer($c, false, null);

            if ($name == false) {
                $data = array('error' => 'There seems to be a problem logging in. We are sorry for the inconvenience.');
                $this->load->view('error', $data);
                return false;
            }
            else {
                $data = array(
                	'cust'		=> '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Hello, ' . $name['firstname'] . '!<b class="caret"></b></strong></a><ul class="dropdown-menu"><li><a href="payment">View Reservations</a></li></ul></li>',
                	'login'		=> '<a href="' . site_url() . 'logout" class="navbar-link">Logout</a>'
                );
            }
		}
		$this->load->view('core/header', $data);
		$this->load->view('core/mainview');
		$this->load->view('footer');
	}
}