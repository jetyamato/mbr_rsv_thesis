<?php

class Privacy extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		session_start();
		if (isset($_SESSION['warn'])) {
			unset($_SESSION['warn']);
		}
	}

	public function index() {
		$this->load->view('privacy');
	}
}