<?php

class WalkIn extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('url','cookie'));
        $this->load->library(array('form_validation','input'));
        $this->load->model(array('LoginModel','ReserveModel','CustomerModel','CmsModel'));
        session_start();
	}
}