<?php

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form','url','cookie'));
		$this->load->library(array('form_validation','input','parser', 'email'));
		$this->load->model(array('LoginModel', 'CustomerModel'));
		session_start();
		if (isset($_SESSION['warn'])) {
			unset($_SESSION['warn']);
		}
	}
	
	public function index() {
		$c = get_cookie('customer');
		if ($c == null) {
			$data = array('alerts' => '');
			$data2 = array('part' => 'Login');
			$this->load->view('login-reg/loginheader', $data2);
			$this->load->view('login-reg/loginview', $data);
		}
		else if ($c != null) {
			$name = $this->CustomerModel->getCustomer($c, false, null);

            if ($name == false) {
                $data = array('error' => 'There seems to be a problem logging in. We are sorry for the inconvenience.');
                $this->load->view('error', $data);
                return false;
            }
            else {
                redirect(base_url());
            }
		}
	}

	public function loginUser() {
		if ($this->input->post('btnlogin')) {
			$config = array(
				array (
					'field'	=> 'username',
					'label'	=> '\'Username\'',
					'rules'	=> 'required|trim|xss_clean'
				),
				
				array (
					'field'	=> 'password',
					'label'	=> '\'Password\'',
					'rules'	=> 'required|trim|xss_clean'
				)
			);
		
			$this->form_validation->set_rules($config);
			
			if ($this->form_validation->run() === FALSE) {
				$data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' . validation_errors() . '</div>');
				$data2 = array('part' => 'Login');
				$this->load->view('login-reg/loginheader', $data2);
				$this->load->view('login-reg/loginview', $data);
				$this->load->view('footer');
			}
			else {
				$result = $this->LoginModel->getUser($this->input->post('username'));
				if ($result == FALSE) {
					$data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>No such username exists in the system.</div>');
					$data2 = array('part' => 'Login');
					$this->load->view('login-reg/loginheader', $data2);
					$this->load->view('login-reg/loginview', $data);
					$this->load->view('footer');
				}
				else {
					$pass = hash("sha512", $this->input->post('password') . $result['salt']);
					if ($result['password'] != $pass) {
						$data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Incorrect password.</div>');
						$data2 = array('part' => 'Login');
						$this->load->view('login-reg/loginheader', $data2);
						$this->load->view('login-reg/loginview', $data);
						$this->load->view('footer');
					}
					else {
						$cookie = array(
							'name'		=> 'customer',
							'value'		=> $result['user_id'],
							'expire'	=> 86400,
							'secure'	=> FALSE
						);
						
						$this->input->set_cookie($cookie);						
						if (isset($_SESSION['access'])):
							$y = $_SESSION['access'];
							unset($_SESSION['access']);
							redirect($y);
						else:
							$_SESSION['alerts'] = '<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Login successful.</div>';
							redirect(base_url());
						endif;
					}
				}
			}
		}
		else if ($this->input->post('btnregister')) {
			$data = array('alerts' => '');
			$data2 = array('part' => 'Register');
			$this->load->view('login-reg/loginheader', $data2);
			$this->load->view('login-reg/registerview', $data);
			$this->load->view('footer');
		}
	}

	public function register() {
		$config = array(
			array (
				'field'	=> 'surname',
				'label'	=> '\'Surname\'',
				'rules'	=> 'required|trim|xss_clean|alpha'
			),

			array (
				'field'	=> 'firstname',
				'label'	=> '\'First Name\'',
				'rules'	=> 'required|trim|xss_clean|alpha'
			),

			array (
				'field'	=> 'middlename',
				'label'	=> '\'Middle Name\'',
				'rules'	=> 'trim|xss_clean|alpha'
			),

			array (
				'field'	=> 'username',
				'label'	=> '\'Username\'',
				'rules'	=> 'required|trim|xss_clean|alpha_dash|min_length[7]|max_length[50]'
			),
			
			array (
				'field'	=> 'password',
				'label'	=> '\'Password\'',
				'rules'	=> 'required|trim|xss_clean|alpha_numeric|min_length[7]'
			),

			array (
				'field'	=> 'cpassword',
				'label'	=> '\'Confirm Password\'',
				'rules'	=> 'required|trim|xss_clean|alpha_numeric|matches[password]'
			),

			array (
				'field'	=> 'email',
				'label'	=> '\'Email\'',
				'rules'	=> 'required|trim|xss_clean|valid_email'
			),

			array (
				'field'	=> 'cemail',
				'label'	=> '\'Confirm Email\'',
				'rules'	=> 'required|trim|xss_clean|valid_email|matches[email]'
			),

			array (
				'field'	=> 'address',
				'label'	=> '\'Address\'',
				'rules'	=> 'required|trim|xss_clean'
			),

			array (
				'field'	=> 'telephone',
				'label'	=> '\'Telephone\'',
				'rules'	=> 'trim|xss_clean|min_length[7]|is_natural'
			),

			array (
				'field'	=> 'cellphone',
				'label'	=> '\'Cellphone\'',
				'rules'	=> 'trim|xss_clean|min_length[11]|is_natural'
			)
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() === FALSE) {
			$data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' . validation_errors() . '</div>');
			$data2 = array('part' => 'Register');
			$this->load->view('login-reg/loginheader', $data2);
			$this->load->view('login-reg/registerview', $data);
			$this->load->view('footer');
		}
		else if ($this->LoginModel->getUser($this->input->post('username')) != false) {
			$data = array('alerts' => '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>There is a customer already registered with that username.</div>');
			$data2 = array('part' => 'Register');
			$this->load->view('login-reg/loginheader', $data2);
			$this->load->view('login-reg/registerview', $data);
			$this->load->view('footer');
		}
		else {
			$salt = $this->generateSalt(512);

			$custdata = array(
				'surname'		=> ucfirst($this->input->post('surname')),
				'firstname'		=> ucfirst($this->input->post('firstname')),
				'middlename'	=> ucfirst($this->input->post('middlename')),
				'email'			=> $this->input->post('cemail'),
				'address'		=> $this->input->post('address'),
				'telephone'		=> $this->input->post('telephone'),
				'cellphone'		=> $this->input->post('cellphone')
			);

			$userdata = array(
				'username'		=> $this->input->post('username'),
				'password'		=> hash('sha512', $this->input->post('cpassword') . $salt),
				'salt'			=> $salt
			);

			$this->LoginModel->registerUser($custdata, $userdata);

			$data = array('salt' => $salt);
			$this->email->from('tamayo209@gmail.com', 'Admin');
			$this->email->to($this->input->post('cemail'));

			$this->email->subject('Marine Beach Resort account activation');
			$this->email->message($this->load->view('login-reg/emailview', $data, true));
			$this->email->send();

			$data = array('email' => $this->input->post('cemail'));
			$this->load->view('login-reg/regsuccess', $data);
		}
	}

	public function logout() {
		delete_cookie('customer');
		session_destroy();
		redirect(base_url());
	}

	public function success() {
		$this->load->view('regsuccess');
	}

	/**
	  * This function generates a password salt as a string of x (default = 15) characters
	  * in the a-zA-Z0-9!@#$%&*? range.
	  * @param $max integer The number of characters in the string
	  * @return string
	  * @author AfroSoft <info@afrosoft.tk>
	  */
	function generateSalt($max = 15) {
	    $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
	    $i = 0;
	    $salt = "";
	    while ($i < $max) {
	        $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
	        $i++;
	    }
	    return $salt;
	}
}	