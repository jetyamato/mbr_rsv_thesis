<?php

class Userman extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('url','form','cookie'));
		$this->load->library(array('input','form_validation'));
		$this->load->model(array('LoginModel','UserModel'));

	}

	public function index() {
		/**$c = get_cookie('staff');
        if ($c != NULL) {
            $s = $this->LoginModel->getUser($c, true, 'staff');

            if ($s == false) {
                $data = array('error' => 'There seems to be a problem logging in. We are sorry for the inconvenience.');
                $this->load->view('error', $data);
                return false;
            }
            else {
            	$this->load->view('backend/header');
            }
        }
        else {
        	$data = array('alerts' => '');**/
        	$this->load->view('backend/header');
        	$rows = $this->UserModel->getUsers();
        	if($rows == false)
        	{
        		$data = array('error' => 'Something Went Wrong. We are sorry for this inconvenience');
        		$this->load->view('error',$data);
        		return false;
        	}
        	$data = array('users'=>$rows);
        	$this->load->view('backend/userman', $data); 
        }
        public function viewuserdetails($userid)
        {
        	if($userid == null)
        	{
        		$data = array('error' => 'Something Went Wrong. We are sorry for this inconvenience');
        		$this->load->view('error',$data);
        		return false;
        	}

        	$user = $this->UserModel->getUser($userid);
        	if($user == false)
        	{
        		$data = array('error' => 'Something Went Wrong. We are sorry for this inconvenience1');
        		$this->load->view('error',$data);
        		return false; 
        	}
        	else
        	{
        		$data = array('user'=>$user,'alerts'=>'');
        		
        		$this->load->view('backend/header');
        		$this->load->view('backend/editview',$data);
        	}
        }
        public function editUser($userid)
        {
        	$config = array(
        		array (
				'field'	=> 'surname',
				'label'	=> '\'Surname\'',
				'rules'	=> 'trim|xss_clean|alpha'
			),

			array (
				'field'	=> 'firstname',
				'label'	=> '\'First Name\'',
				'rules'	=> 'trim|xss_clean|alpha'
			),

			array (
				'field'	=> 'middlename',
				'label'	=> '\'Middle Name\'',
				'rules'	=> 'required|trim|xss_clean|alpha'
			),

			array (
				'field'	=> 'username',
				'label'	=> '\'Username\'',
				'rules'	=> 'trim|xss_clean|alpha_dash|min_length[7]|max_length[50]'
			),
			
			array (
				'field'	=> 'password',
				'label'	=> '\'Password\'',
				'rules'	=> 'trim|xss_clean|alpha_numeric|min_length[7]'
			),

			array (
				'field'	=> 'cpassword',
				'label'	=> '\'Confirm Password\'',
				'rules'	=> 'trim|xss_clean|alpha_numeric|matches[password]'
			),

			array (
				'field'	=> 'email',
				'label'	=> '\'Email\'',
				'rules'	=> 'trim|xss_clean|valid_email'
			),

			array (
				'field'	=> 'address',
				'label'	=> '\'Address\'',
				'rules'	=> 'trim|xss_clean'
			),

			array (
				'field'	=> 'telephone',
				'label'	=> '\'Telephone\'',
				'rules'	=> 'trim|xss_clean|min_length[7]|is_natural'
			),

			array (
				'field'	=> 'cellphone',
				'label'	=> '\'Cellphone\'',
				'rules'	=> 'trim|xss_clean|min_length[11]|is_natural'
			)
        	);
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() === false)
			{
                $errors = array();
				$errors[] = validation_errors();
                $data['alerts'] = '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' . implode($errors) . '</div>';
                $this->load->view('backend/header');
                $this->load->view('backend/editview',$data);
                return false;

			}
        }
	}
