<?php

class Cms extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('url','form','cookie'));
		$this->load->library(array('input','form_validation'));
		$this->load->model(array('StaffModel','UserModel','RoomModel','CmsModel'));

	}

	public function index() {
    	$c = get_cookie('staff');
        if ($c != NULL) {
            $s = $this->StaffModel->getStaffInfo($c);

            if ($s == false) {
                $data = array('error' => 'There seems to be a problem logging in. We are sorry for the inconvenience.');
                $this->load->view('error', $data);
                return false;
            }
            else {
            	$this->load->view('backend/cmsviews/header');
                $this->load->view('backend/cms');
            }
        }
        else {
            redirect(base_url() . 'staff');
        }
    }

    public function rooms() {
        $roomslist = null;
        $w = 0;

        $rooms = $this->RoomModel->getRooms();
        if ($rooms == false):
            $roomslist = 'There are no rooms in the database.';
        else:
            foreach ($rooms as $r): 
                $content = $this->CmsModel->getContent($r['room_id']);
                if ($content == false):
                    $data = array(
                        'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                    );
                    $this->load->view('error', $data);
                    return false;
                endif;
                $data = array(
                    'c'     => $content,
                    'room'  => $r,
                    'w'     => $w
                );
                $roomslist .= $this->load->view('backend/cmsviews/roomslist', $data, true);
                $w++;
            endforeach;
        endif;

        $data = array('roomslist' => $roomslist);
        $this->load->view('backend/cmsviews/rooms', $data);
    }

    public function news() {
        $this->load->view('backend/cmsviews/news');
    }

    public function announcements() {
        $this->load->view('backend/cmsviews/announcements');
    }

    public function gallery() {
        $this->load->view('backend/cmsviews/gallery');
    }

    public function desc() {
        $this->load->view('backend/cmsviews/desc');
    }

    public function contact() {
        $this->load->view('backend/cmsviews/contact');
    }

    public function terms() {
        $this->load->view('backend/cmsviews/terms');
    }

    public function privacy() {
        $this->load->view('backend/cmsviews/privacy');
    }

    public function help() {
        $this->load->view('backend/cmsviews/help');
    }

    public function viewEdit($ctype, $id) {
        switch ($ctype):
            case 'rooms':
                $curimg = $this->CmsModel->getContent($id);
                $room = $this->RoomModel->getRooms($id);
                $roomtype = $this->RoomModel->getType();
                if ($curimg == false || $room == false || $roomtype == false):
                    $data = array(
                        'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                    );
                    $this->load->view('error', $data);
                    return false;
                endif;
                $data = array(
                    'id' => $id,
                    'curimg' => $curimg[0]['content'],
                    'room' => $room[0],
                    'roomtype' => $roomtype
                );
                $this->load->view('backend/cmsviews/editroomview', $data);
                break;
        endswitch;
    }

    public function edit($ctype, $id) {
        $errors = array();
        if ($ctype == 'rooms'):
            $config = array(
                array(
                    'field' => 'roomname',
                    'label' => 'Room Name',
                    'rules' => 'required|trim|xss_clean|callback_alphanumspace'
                ),
                array(
                    'field' => 'roomtype',
                    'label' => 'Room Type',
                    'rules' => 'callback_validroomtype'
                ),
                array(
                    'field' => 'roomdesc',
                    'label' => 'Description',
                    'rules' => 'required|trim|xss_clean'
                ),
                array(
                    'field' => 'remarks',
                    'label' => 'Remarks',
                    'rules' => 'required|trim|xss_clean|alpha_numeric'
                ),
                array(
                    'field' => 'roomstatus',
                    'label' => 'Status',
                    'rules' => 'callback_validroomstatus'
                )
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() === false):
                $errors[] = validation_errors();
                echo json_encode(array('success' => false, 'errors' => $errors));
                return false;
            else:
                $config = array();
                $info = $this->RoomModel->getTypeInfo($this->input->post('roomtype'));

                if ($info == false):
                    $data = array(
                        'error'       => 'Something went wrong. We are sorry for this inconvenience.'
                    );
                    $this->load->view('error', $data);
                endif;
                switch ($this->input->post('roomtype')):
                    case 1:
                        $config['upload_path'] = 'pics/new/standard2/';
                        break;
                    case 2:
                        $config['upload_path'] = 'pics/new/standard6/';
                        break;
                    case 3:
                        $config['upload_path'] = 'pics/new/dorm/';
                        break;
                    case 4:
                        $config['upload_path'] = 'pics/new/family/';
                        break;
                endswitch;
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('imginput')):
                    $errors[] = $this->upload->display_errors();
                    echo json_encode(array('success' => false, 'errors' => $errors));
                    return false;
                endif;

                $updata = $this->upload->data();
                $this->RoomModel->updateRoom($id, $info[0]['room_capacity_id'], $this->input->post('roomtype'), $this->input->post('roomname'), $this->input->post('roomdesc'), $this->input->post('remarks'), $this->input->post('roomstatus'), $config['upload_path'] . $updata['file_name'], $ctype);

                echo json_encode(array('success' => true));
            endif;
        endif;
    }

    public function getRoomTypeInfo($typeid) {
        $info = $this->RoomModel->getTypeInfo($typeid);

        if ($info == false):
            $data = array(
                'error'       => 'Something went wrong. We are sorry for this inconvenience.'
            );
            $this->load->view('error', $data);
        endif;

        echo '<strong>Minimum Capacity: </strong>' . ($info[0]['min_capacity'] > 1 ? $info[0]['min_capacity'] . ' persons' : $info[0]['min_capacity'] . ' person') . '<br /><strong>Maximum Capacity: </strong>' . ($info[0]['max_capacity'] > 1 ? $info[0]['max_capacity'] . ' persons' : $info[0]['max_capacity'] . ' person') . '<br /><strong>Price per day: </strong>' . $info[0]['roomrate'];
    }

    public function alphanumspace($rname) {
        $validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ";
        $vc = str_split($validChars);
        $splitChars = str_split($rname);
        foreach ($splitChars as $s):
            if (array_search($s, $vc) == false):
                $this->form_validation->set_message('alphanumspace', 'Room Name must consist only of alphanumeric characters and spaces.');
                return false;
            endif;
        endforeach;
        return true;
    }

    public function validroomtype($rtype) {
        if ($rtype == 'none'):
            $this->form_validation->set_message('validroomtype', 'You must pick a Room Type.');
            return false;
        endif;
        return true;
    }

    public function validmincap($mincap) {
        if ($mincap == 'none'):
            $this->form_validation->set_message('validmincap', 'Minimum Capacity must be greater than zero.');
            return false;
        endif;
        return true;
    }

    public function validmaxcap($maxcap) {
        if ($maxcap == 'none'):
            $this->form_validation->set_message('validmaxcap', 'Maximum Capacity must be greater than zero.');
            return false;
        endif;
        return true;
    }

    public function validroomstatus($status) {
        if ($status == 'none'):
            $this->form_validation->set_message('validroomstatus', 'You must pick a Room Status.');
            return false;
        endif;
        return true;
    }

    public function validroomrate($rate) {
        if ($rate < 1):
            $this->form_validation->set_message('validroomrate', 'Room Rate must be greater than or equal to 1.');
            return false;
        endif;
        return true;
    }
}