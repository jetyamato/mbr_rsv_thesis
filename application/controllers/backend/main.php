<?php

class Main extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('url','form','cookie'));
		$this->load->library(array('input','form_validation'));
		$this->load->model(array('StaffModel'));
	}

	public function index() {
		$c = get_cookie('staff');
        if ($c != NULL) {
            $s = $this->StaffModel->getStaffInfo($c);

            if ($s == false) {
                $data = array('error' => 'There seems to be a problem logging in. We are sorry for the inconvenience.');
                $this->load->view('error', $data);
                return false;
            }
            else {
            	$this->load->view('backend/header');
            }
        }
        else {
        	$data = array('alerts' => '');
        	$this->load->view('backend/cleanheader');
        	$this->load->view('backend/mainview', $data);
        }
	}

	public function loginUser() {
		$config = array(
			array (
				'field'	=> 'username',
				'label'	=> '\'Username\'',
				'rules'	=> 'required|trim|xss_clean'
			),
			
			array (
				'field'	=> 'password',
				'label'	=> '\'Password\'',
				'rules'	=> 'required|trim|xss_clean'
			)
		);
	
		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() === FALSE) {
			$data = array(
				'alerts'	=> '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' . validation_errors() . '</div>'
			);
			$this->load->view('backend/cleanheader');
        	$this->load->view('backend/mainview', $data);
		}
		else {
			$result = $this->StaffModel->getStaff($this->input->post('username'));
			if ($result == FALSE) {
				$data = array(
					'alerts'	=> '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>No such username exists in the system.</div>'
				);
				$this->load->view('backend/cleanheader');
	        	$this->load->view('backend/mainview', $data);
			}
			else {
				$pass = hash("sha512", $this->input->post('password') . $result['salt']);
				if ($result['password'] != $pass) {
					$data = array(
						'alerts'	=> '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Incorrect password.</div>'
					);
					$this->load->view('backend/cleanheader');
		        	$this->load->view('backend/mainview', $data);
				}
				else {
					$cookie = array(
						'name'		=> 'staff',
						'value'		=> $result['user_id'],
						'expire'	=> 900,
						'secure'	=> FALSE
					);
					
					$this->input->set_cookie($cookie);
					redirect('staff');
				}
			}
		}
	}

	public function logout() {
		$cookie = array(
			'name'		=> 'staff',
			'expire'	=> (time() - 86400),
		);

		set_cookie($cookie);

		redirect('staff');
	}
}