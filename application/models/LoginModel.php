<?php

class LoginModel extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
	
	public function getUser($user, $selectById = false, $usertype = 'customer') {
		if ($selectById) {
			$this->db->where('user_id', $user);
		}
		else {
			$this->db->where('username', $user);
		}
		
		$this->db->where('usertype', $usertype);
		
		$query = $this->db->get('users');

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->row_array();
		}
	}

	public function getSalt($usertype) {
		$this->db->select('salt');
		$this->db->where('usertype', $usertype);
		$query = $this->db->get('users');

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->row_array();
		}
	}

	public function registerUser($custData, $userData) {
		$this->db->insert('users', $userData);
		$custData['user_id'] = $this->db->insert_id();
		$this->db->insert('customers', $custData);
	}
}