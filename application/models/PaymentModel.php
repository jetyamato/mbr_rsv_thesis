<?php

class PaymentModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function updateTrans($rsvid, $ppid, $remarks) {
		$data = array('paypal_id' => $ppid, 'remarks' => $remarks);
		$this->db->update('transactions', $data, array('rsv_id' => $rsvid));
	}

	public function createTrans($rsvid, $amount, $transtype) {
		$sql = "INSERT INTO transactions (rsv_id,amount_paid,trans_date,trans_type) VALUES (?,?,NOW(),?)";
		$this->db->query($sql, array($rsvid,$amount,$transtype));
	}

	public function getBreakdown($rid) {
		$sql = "SELECT rsv_id,cust_id,no_adults,no_children,swim_sched,
			if(month(arrival) in (3,4,5), 'Peak Season', 'Regular Season') AS season,
			if(swim_sched='Daytime',
			(SELECT daytime_fee FROM reservation.entrance_fee WHERE age_type = 'ADULT' and fee_type = 'REGULAR'),
			(SELECT overnight_fee FROM reservation.entrance_fee WHERE age_type = 'ADULT' and fee_type = 'REGULAR')) as adult_regular_rate,
			if(swim_sched='Daytime',
			(SELECT daytime_fee FROM reservation.entrance_fee WHERE age_type = 'CHILDREN' and fee_type = 'REGULAR'),
			(SELECT overnight_fee FROM reservation.entrance_fee WHERE age_type = 'CHILDREN' and fee_type = 'REGULAR')) as children_regular_rate,
			if(swim_sched='Daytime',
			(SELECT daytime_fee FROM reservation.entrance_fee WHERE age_type = 'ADULT' and fee_type = 'SEASONAL'),
			(SELECT overnight_fee FROM reservation.entrance_fee WHERE age_type = 'ADULT' and fee_type = 'SEASONAL')) as adult_seasonal_rate,
			if(swim_sched='Daytime',
			(SELECT daytime_fee FROM reservation.entrance_fee WHERE age_type = 'CHILDREN' and fee_type = 'SEASONAL'),
			(SELECT overnight_fee FROM reservation.entrance_fee WHERE age_type = 'CHILDREN' and fee_type = 'SEASONAL')) as children_seasonal_rate,
			(no_adults * if(month(arrival) in (3,4,5),(SELECT adult_seasonal_rate),(SELECT adult_regular_rate))) as adult_subtotal,
			(no_children * if(month(arrival) in (3,4,5),(SELECT children_seasonal_rate),(SELECT children_regular_rate))) as children_subtotal,
			room.room_id, roomrate, DATEDIFF(departure, arrival) + 1 AS num_days, (SELECT roomrate) * (SELECT num_days) AS room_subtotal, (SELECT adult_subtotal) + (SELECT children_subtotal) + (SELECT room_subtotal) AS grand_total
			FROM rsv_master LEFT JOIN (SELECT * FROM room_master LEFT JOIN rsv_rooms USING (room_id)) AS room USING (rsv_id) WHERE rsv_id = ?";
		$query = $this->db->query($sql, array($rid));

		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}

	public function getRsvRooms($rid) {
		$query = $this->db->get_where('rsv_rooms', array('rsv_id' => $rid));

		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}

	public function getRsvDetails($cid) {
		$this->db->select('rsv_master.rsv_id,cust_id,no_adults,no_children,swim_sched,arrival,departure,grand_total,rsv_date,pay_method,rsvstatus,rsv_code,room_id');
		$this->db->join('rsv_rooms', 'rsv_master.rsv_id = rsv_rooms.rsv_id');
		$this->db->group_by('rsv_id');
		$query = $this->db->get_where('rsv_master', array('cust_id' => $cid));

		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}

	public function getRsv($rsvid) {
		$query = $this->db->get_where('rsv_master', array('rsv_id' => $rsvid));

		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}
}