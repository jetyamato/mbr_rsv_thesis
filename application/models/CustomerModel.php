<?php

class CustomerModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function getCustomer($custID = null, $verify = false, $salt = null) {
		if ($verify) {
			$this->db->select('salt, users.user_id, cust_status');
			$this->db->from('users');
			$this->db->join('customers', 'users.user_id = customers.user_id', 'left');
			$this->db->where(array('salt' => $salt, 'cust_status' => 'pending'));
			$query = $this->db->get();
		}
		else {
			$this->db->where('user_id', $custID);
			$query = $this->db->get('customers');
		}

		if ($query->num_rows() == 0) {
			return false;
		}
		else {
			return $query->row_array();
		}
	}

	public function setCustomer($userID) {
		$this->db->where('user_id', $userID);
		$this->db->update('customers', array('cust_status' => 'confirmed'));
	}
}