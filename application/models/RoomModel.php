<?php

class RoomModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function updateRoom($roomid, $roomcap, $roomname, $realname, $roomdesc, $remarks, $roomstatus, $filepath, $ctype) {
		$data = array(
			'room_capacity_id'	=> $roomcap,
			'room_name_id'		=> $roomname,
			'real_name'			=> $realname,
			'roomdesc'			=> $roomdesc,
			'remarks'			=> $remarks,
			'roomstatus'		=> $roomstatus
		);
		$this->db->where('room_id', $roomid);
		$this->db->update('rooms', $data);

		$data = array(
			'content'			=> $filepath
		);

		$this->db->where(array('related_to' => $ctype, 'source_id' => $roomid));
		$this->db->update('cmsrooms', $data);
	}

	public function getRooms($roomid = null) {
		if ($roomid == null):
			$query = $this->db->get('room_master');
		else:
			$query = $this->db->get_where('room_master', array('room_id' => $roomid));
		endif;

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getType() {
		$query = $this->db->get('room_name');

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getTypeInfo($typeid) {
		$this->db->select('room_capacity.room_capacity_id, min_capacity, max_capacity, roomrate');
		$this->db->join('rooms', 'room_capacity.room_capacity_id = rooms.room_capacity_id', 'left');
		$this->db->limit(1);
		$query = $this->db->get_where('room_capacity', array('rooms.room_name_id' => $typeid));

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}
}