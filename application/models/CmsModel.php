<?php

class CmsModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function getContent($sid = null) {
		if ($sid != null) {
			$query = $this->db->get_where('cmsrooms', array('source_id' => $sid));
		}
		else {
			$query = $this->db->get('cmsrooms');
		}

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getSpecContent($sid, $ctype) {
		$query = $this->db->get_where('cmsrooms', array('source_id' => $sid, 'contentname' => $ctype));

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}
}