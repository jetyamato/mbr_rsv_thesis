<?php

class UserModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function getUsers() {
		$query = $this->db->get_where('users', array('usertype' => 'customer'));

		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}

	public function getUser($userid) {
		$this->db->join('users', 'customers.user_id = users.user_id', 'left');
		$query = $this->db->get_where('customers', array('users.user_id' => $userid));

		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}
}