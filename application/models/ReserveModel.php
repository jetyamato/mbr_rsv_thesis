<?php

class ReserveModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function checkValidDate($checkin) {
		foreach ($_SESSION['rsvdata'] as $room):
			$this->db->join('rsv_master', 'rsv_rooms.rsv_id = rsv_master.rsv_id');
			$query = $this->db->get_where('rsv_rooms', array('rsv_rooms.room_id' => $room, 'rsvstatus' => 'Confirmed'));

			$rows = $query->result_array();

			if (count($rows) > 0):
				foreach ($rows as $r):
					$inputcheckin = strtotime($checkin);
					$arrival = strtotime($r['arrival']);
					$departure = strtotime($r['departure']);

					if ($inputcheckin >= $arrival && $inputcheckin < $departure):
						return false;
					endif;
				endforeach;
			endif;

			return true;
		endforeach;
	}

	public function getRoomType() {
		$query = $this->db->get('room_name');
		if ($query->num_rows == 0):
			return false;
		else:
			return $query->result_array();
		endif;
	}

	public function getCapacity($capacity) {
		$query = $this->db->get_where('room_capacity', array('room_capacity_desc' => $capacity));
		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function createRsv($custid, $adults, $kids, $sched, $checkin, $checkout, $rsvdate, $rooms, $method, $tcode, $remarks) {

		$cm = date('F', mktime());
		$row = null;

		switch ($cm) {
			case 'March':
			case 'April':
			case 'May':
				if ($sched == 'day'):
					$this->db->select('age_type, daytime_fee AS fee');
					$query = $this->db->get_where('entrance_fee', array('fee_type' => 'Seasonal'));
					$row = $query->result_array();
				elseif ($sched == 'night'):
					$this->db->select('age_type, overnight_fee AS fee');
					$query = $this->db->get_where('entrance_fee', array('fee_type' => 'Seasonal'));
					$row = $query->result_array();
				endif;
				break;
			default:
				if ($sched == 'day'):
					$this->db->select('age_type, daytime_fee AS fee');
					$query = $this->db->get_where('entrance_fee', array('fee_type' => 'Regular'));
					$row = $query->result_array();
				elseif ($sched == 'night'):
					$this->db->select('age_type, overnight_fee AS fee');
					$query = $this->db->get_where('entrance_fee', array('fee_type' => 'Regular'));
					$row = $query->result_array();
				endif;
				break;
		};

		$grandTotal = null;

		foreach ($row as $r):
			if ($r['age_type'] == 'Adult'):
				$grandTotal += $adults * $r['fee'];
			elseif ($r['age_type'] == 'Children'):
				$grandTotal += $kids * $r['fee'];
			endif;
		endforeach;

		foreach ($rooms as $r):
			$query = $this->db->get_where('room_master', array('room_id' => $r));
			$row = $query->row();
			$days = date_diff(date_create($checkin), date_create($checkout), true);
			$grandTotal += $row->roomrate * ($days->d + 1);
		endforeach;

		$data = array(
			'cust_id'		=> $custid,
			'no_adults'		=> $adults,
			'no_children'	=> $kids,
			'swim_sched'	=> ($sched == 'day' ? 'Daytime' : 'Overnight'),
			'arrival'		=> $checkin,
			'departure'		=> $checkout,
			'grand_total'	=> $grandTotal,
			'rsv_date'		=> $rsvdate,
			'pay_method'	=> $method,
			'rsv_code'		=> $tcode,
			'remarks'		=> $remarks
		);

		$this->db->insert('rsv_master', $data);

		$data = array(
			'rsv_id' => $this->db->insert_id()
		);

		foreach ($rooms as $r):
			$data['room_id'] = $r;
			$this->db->insert('rsv_rooms', $data);
		endforeach;
	}

	public function getPaymentMethods() {
		$query = $this->db->get('payment_methods');

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getFacilitiesRefined() {
		$this->db->order_by('facility_id');
		$this->db->group_by('description');
		$this->db->where('facility_id NOT IN(SELECT facility_id FROM rsv_facilities)');
		$query = $this->db->get('fac_master');

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getRoom($room_id) {
		$query = $this->db->get_where('room_master', array('room_id' => $room_id));

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getRoomsRefined($params = null, $filter = null, $blacklist = null) {
		if (is_array($params) && $blacklist == true):
			if ($filter != null):
				$this->db->where("(room_id NOT IN(" . implode(',', $params) . ") AND roomstatus = 'Available') AND room_name_desc = '" . $filter . "'");
				$query = $this->db->get('room_master');
			else:
				$this->db->where("room_id NOT IN(" . implode(',', $params) . ") AND roomstatus = 'Available'");
				$query = $this->db->get('room_master');
			endif;
		elseif (is_string($params) && $params != null):
			$this->db->where("roomstatus = 'Available' AND room_name_desc = '" . $params . "'");
			$query = $this->db->get('room_master');
		elseif (is_int($params) && $params != null):
			$this->db->where("roomstatus = 'Available' AND room_id = " . $params);
			$query = $this->db->get('room_master');
		else:
			if ($filter != null):
				$this->db->where("roomstatus = 'Available' AND room_name_desc = '" . $filter . "'");
				$query = $this->db->get('room_master');
			else:
				$this->db->where("roomstatus = 'Available'");
				$query = $this->db->get('room_master');
			endif;
		endif;

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function countRoomsRefined($capacity) {
		$query = $this->db->query("select (select count(*) from room_master where room_capacity_desc = '" . $capacity . "') - (select count(*) from rsv_rooms where room_type = '" . $capacity . "') as units;");

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}

	}

	public function countFacsRefined($desc) {
		$this->db->where('description = \'' . $desc . '\' AND facility_id NOT IN(SELECT facility_id FROM rsv_rooms)');
		$query = $this->db->get('fac_master');

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->num_rows;
		}

	}

	public function getFacilities($offset) {
		$this->db->join('(SELECT fac_type,reg_price,seasonal_price FROM facility_price) AS a','facilities.description=a.fac_type');
		$query = $this->db->get_where('facilities', array('fac_status' => 'Available'), 5, $offset);

		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}

	public function getEntranceFee($adults, $kids) {
		$query = $this->db->query("SELECT (SELECT " . $adults . " * reg_fee FROM entrance_fee WHERE age_type='Adult') + (SELECT " . $kids . " * reg_fee FROM entrance_fee WHERE age_type='Children')");
		
		if ($query->num_rows == 0) {
			return false;
		}
		else {
			return $query->result_array();
		}
	}
}