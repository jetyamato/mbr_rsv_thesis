<?php

class StaffModel extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}

	public function getStaff($username) {
		$this->db->where("username = '$username' AND usertype IN('staff', 'admin')");
		$query = $this->db->get('users');

		if ($query->num_rows() == 0) {
			return false;
		}
		else {
			return $query->row_array();
		}
	}

	public function getStaffInfo($staffid) {
		$query = $this->db->get_where('users', array('user_id' => $staffid));

		if ($query->num_rows() == 0) {
			return false;
		}
		else {
			return $query->row_array();
		}
	}
}