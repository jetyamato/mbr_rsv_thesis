<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "welcome";
//$route['404_override'] = '';
$route['login'] = "login";
$route['logout'] = "login/logout";
$route['default_controller'] = "main";
$route['about'] = "about";
$route['reserve'] = "reserve";
$route['payment'] = "payment";
$route['privacy'] = "privacy";
$route['modal/(:any)'] = "modal/loadModal/$1";
$route['verify/(:any)'] = "verify/verifySalt/$1";
$route['staff'] = "backend/main";
$route['staff/logout'] = "backend/main/logout";
$route['staff/cms'] = "backend/cms";
$route['staff/cms/(:any)'] = "backend/cms/$1";
$route['staff/cms/getRoomTypeInfo/(:num)'] = "backend/cms/getRoomTypeInfo/$1";
$route['staff/cms/viewEdit/(:any)/(:num)'] = "backend/cms/viewEdit/$1/$2";
$route['staff/cms/edit/(:any)/(:num)'] = "backend/cms/edit/$1/$2";
$route['statistics'] = "statistics";
$route['strack'] = "strack";
$route['clear'] = "clearrsv";
/* End of file routes.php */
/* Location: ./application/config/routes.php */