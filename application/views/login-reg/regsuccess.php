<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/css/bootstrap.css" />
</head>

<body>
<main>
	<div class="container">
		<h2>Registration Successful</h2>
		<p>An email has been sent to <strong><?php echo $email; ?></strong>. Please check your email for the activation link.</p>
	</div>
</main>
</body>
</html>