<main>
	<div class="container">
		<?php echo $alerts; ?>
		<h1>Registration (Fields marked with a * are required.)</h1>
		<div class="well">
			<?php echo form_open('login/register', array('class' => 'form-horizontal')); ?>
				<div class="container">
					<div class="row">
						<div class="form-group">
							<label for="surname" class="col-sm-1 control-label">*Surname</label>
							<div class="col-sm-4">
							<input type="text" name="surname" class="form-control" />
							</div>

							<label for="email" class="col-sm-1 control-label">*Email</label>
							<div class="col-sm-4">
							<input type="text" name="email" class="form-control" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<label for="firstname" class="col-sm-1 control-label">*First Name</label>
							<div class="col-sm-4">
							<input type="text" name="firstname" class="form-control" />
							</div>

							<label for="cemail" class="col-sm-1 control-label">*Confirm Email</label>
							<div class="col-sm-4">
							<input type="text" name="cemail" class="form-control" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<label for="middlename" class="col-sm-1 control-label">Middle name</label>
							<div class="col-sm-4">
							<input type="text" name="middlename" class="form-control" />
							</div>

							<label for="address" class="col-sm-1 control-label">*Address</label>
							<div class="col-sm-4">
							<input type="text" name="address" class="form-control" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<label for="username" class="col-sm-1 control-label">*Username</label>
							<div class="col-sm-4">
							<input type="text" name="username" class="form-control" />
							</div>

							<label for="telephone" class="col-sm-1 control-label">Telephone</label>
							<div class="col-sm-4">
							<input type="text" name="telephone" class="form-control" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<label for="password" class="col-sm-1 control-label">*Password</label>
							<div class="col-sm-4">
							<input type="password" name="password" class="form-control" />
							</div>

							<label for="cellphone" class="col-sm-1 control-label">Cellphone</label>
							<div class="col-sm-4">
							<input type="text" name="cellphone" class="form-control" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<label for="cpassword" class="col-sm-1 control-label">*Confirm Password</label>
							<div class="col-sm-4">
							<input type="password" name="cpassword" class="form-control" />
							</div>
							<div class="col-sm-2 col-sm-offset-3">
							<input type="submit" name="btnsubmit" id="btnsubmit" class="btn-sm btn-primary" value="Submit" />
							<input type="reset" name="btnclear" id="btnclear" class="btn-sm" value="Clear Fields" />
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
