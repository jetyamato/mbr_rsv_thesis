<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/css/bootstrap.css" />
</head>
<body>
	<h1>Registration Confirmation</h1>
	<p>An email has been sent to this address. If you did not request this message, you can safely ignore this.</p>
	<p>Click this link to activate your account:</p>
	<p><a href="http://localhost/redesign/verify/<?php echo $salt; ?>">Activate your account</a></p>
</body>
</html>