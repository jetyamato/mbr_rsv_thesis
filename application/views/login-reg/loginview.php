<main>
	<div class="container">
		<?php echo $alerts; ?>
		<div class="col-sm-6">
		<h1>Customer Login</h1>
		<div class="well">
			<?php echo form_open('login/loginUser', array('class' => 'form-horizontal')); ?>
				<div class="container">
					<div class="form-group">
						<label for="username" class="col-sm-1 control-label">Username</label>
						<div class="col-sm-4">
						<input type="text" name="username" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-sm-1 control-label">Password</label>
						<div class="col-sm-4">
						<input type="password" name="password" class="form-control" />
						</div>
					</div>
					<div class="col-sm-offset-1">
						<input type="submit" name="btnlogin" id="btnlogin" class="btn-sm btn-primary" value="Login to Website" />
						<input type="reset" name="btnreset" id="btnclear" class="btn-sm btn-primary" value="Clear Fields" />
					</div>
				</div>
			</form>
		</div>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-5">
			<h1>Register</h1>
			<div class="well">
				<p>Don&apos;t have a Facebook account?
				<?php echo form_open('login/loginUser', array('class' => 'form-horizontal')); ?>
					<input type="submit" name="btnregister" id="btnregister" class="btn-sm btn-primary" value="Click here to Register" />
				</form>
			</div>
		</div>
	</div>
</main>
