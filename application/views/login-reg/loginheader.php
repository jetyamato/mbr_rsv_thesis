<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Marine Beach Resort</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom.css" />
	<script src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script src="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<p class="navbar-brand"><a href="<?php echo base_url(); ?>">Marine Beach Resort</a> &gt; <?php echo $part; ?></p>
		</div>
	</div>
</nav>
