			<script>
				$(document).ready(function() {
					$("a[href='#rooms']").click(function() {
						$('#inner').load('<?php echo base_url(); ?>reserve/loadRoomsRightPane');
					});
					$("a[href='#others']").click(function() {
						$('#inner').load('<?php echo base_url(); ?>reserve/loadFacsRightPane');
					});
				});
			</script>
			<div class="row">
				<div class="col-sm-12">
					<table class="table table-condensed table-bordered">
						<tr>
							<td>
								<img src="<?php echo base_url(); ?>pics/potato.png" />
								<p>Click the image above<br />to enlarge this image.</p>
							</td>
							<td>
								<table>
								<tr><td><strong>Facility Type:</strong></td></tr>
								<tr><td><?php echo $facs['description']; ?></td></tr>
								<tr><td><strong>Remarks:</strong></td></tr>
								<tr><td><?php echo $facs['remarks']; ?></td></tr>
								<tr><td><strong>Status:</strong></td></tr>
								<tr><td><?php echo $facs['fac_status']; ?></td></tr>
								<tr><td><strong>Price per hour:</strong></td></tr>
								<tr><td><strong>PHP</strong> <?php echo $facs['reg_price']; ?></td></tr>
								</table>
							</td>
							<td>
								<table>
								<?php echo form_open('reserve/createReserve/' . $facs['facility'], array('id' => $facs['facility'], 'class' => 'form-horizontal')); ?>
								<tr>
									<td colspan="2"><strong><?php echo $u . ($u > 1 ? ' Units Available' : ' Unit Available'); ?></strong><br /><br /></td>
								</tr>
								<tr>
									<td><strong>Number of Units to Acquire:&nbsp;&nbsp;</strong></td>
									<td>
										<div class="form-group">
										<select name="numberoffacs" class="form-control">
										<?php
											for ($i = 1; $i <= $u; $i++):
												echo '<option value="' . $i . '">' . $i . '</option>';
											endfor;
										?>
										</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><input type="submit" name="btnrsv" id="btnrsv" class="btn btn-primary" value="Select this Facility" /></td>
								</tr>
								<?php echo form_close(); ?>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
