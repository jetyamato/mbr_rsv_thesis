<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/fancybox/source/jquery.fancybox.css?v=2.1.5" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>datepair/lib/bootstrap-datepicker.css" />
<script src="<?php echo base_url(); ?>js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?php echo base_url(); ?>datepair/lib/bootstrap-datepicker.js"></script>
<script>
	$(document).ready(function() {
		var dt = new Date();
		$('a.fancybox').fancybox();
		$('.date').datepicker({
			'format': 'MM d, yyyy',
			'autoclose': true,
			'startDate': dt
		});
		$('button.btn-default').click(function() {
			$('.date').datepicker('show');
		});
		$('input:radio[name="rbtonsite"]').change(function() {
			if ($(this).is(':checked'))
				$('#paymentmethod').attr('disabled', true);

			if ($(this).val() == 'walkin') {
				$('input:radio[name="sched"]').attr('disabled', true);
				$('#datecheckin').attr('disabled', true);
				$('.date').datepicker('remove');
				$('.btn-default').attr('disabled', true);
			}
			else {
				$('input:radio[name="sched"]').removeAttr('disabled');
				$('#datecheckin').removeAttr('disabled');
				$('.date').datepicker({
					'format': 'MM d, yyyy',
					'autoclose': true,
					'startDate': dt
				});
				$('.btn-default').removeAttr('disabled');
			}
		});
		$('#btnok').click(function() {
			$('#rsvdetails').submit();
		});
		$('#btnreload').click(function() {
			window.location.href = '<?php echo base_url(); ?>reserve';
		});
	});
</script>
<main>
	<?php echo $alerts; ?>
	<nav class="col-sm-4">
		<ul class="nav nav-stacked">
		<li><h2>Your Selected Rooms</h2></li>
		<li>
			<div class="well">
			<?php
				if (count($_SESSION['rsvdata'] > 0)):
					foreach ($room as $r):
						foreach ($_SESSION['rsvdata'] as $r2):
							if ($r['room_id'] == $r2):
								echo form_open('reserve/createReserve/remove/' . $r2, array('class' => 'form-horizontal'));
								echo '<p><strong>' . $r['room_name_desc'] . ' (' . $r['min_capacity'] . ' - ' . $r['max_capacity'] . ' persons): ' . $r['real_name'] . '</strong></p>';
								echo '<input type="submit" name="btnrmv" id="btnrmv" class="btn btn-xs btn-danger" value="Remove" />';
								echo form_close('<br />');
							endif;
						endforeach;
					endforeach;
				endif;
			?>
			</div>
		</li>
		<hr />
		<li><h2>Reservation Details</h2></li>
		<?php echo form_open('reserve/createReserve', array('id' => 'rsvdetails', 'class' => 'form-horizontal')); ?>
			<div class="form-group">
				<li><label class="control-label">Swimming Schedule</label></li>
				<li><label><input type="radio" name="sched" id="sched" value="day" <?php echo ($this->input->post('sched') == 'day' ? 'checked' : ''); ?><?php echo ($this->input->post('rbtonsite') == 'walkin' ? 'disabled' : ''); ?> /> Daytime Swimming (8:00 AM - 5:00 PM)</label></li>
				<li><label><input type="radio" name="sched" id="sched" value="night" <?php echo ($this->input->post('sched') == 'night' ? 'checked' : ''); ?><?php echo ($this->input->post('rbtonsite') == 'walkin' ? 'disabled' : ''); ?> /> Overnight Swimming (5:00 PM - 2:00 AM)</label></li>
				<li><label class="control-label">Number of Adults</label></li>
				<li><select name="numberofadults" class="form-control">
					<option value="none" <?php echo set_select('numberofadults', 'none'); ?>>---Pick number---</option>
					<?php
						for ($x = 1; $x <= 70; $x++):
							echo '<option value="' . $x . '"' . set_select('numberofadults', $x) . '>' . $x . '</option>';
						endfor;
					?>
				</select></li>
				<li><span class="help-block">Adults are persons who are beyond 11 years old.</span></li>
				
				<li><label class="control-label">Number of Kids</label></li>
				<li><select name="numberofkids" class="form-control">
					<option value="none" <?php echo set_select('numberofkids', 'none'); ?>>---Pick number---</option>
					<?php
						for ($x = 1; $x <= 70; $x++):
							echo '<option value="' . $x . '"' . set_select('numberofkids', $x) . '>' . $x . '</option>';
						endfor;
					?>
				</select></li>
				<li><span class="help-block">Kids are persons who are 5 to 11 years old. You may leave this field blank if you don't have kids to take with you.</span></li>

				<li><label for="datecheckin" class="control-label">Date of check-in</label></li>
				<li>
					<div class="input-group">
						<input type="text" name="datecheckin" id="datecheckin" class="date form-control" value="<?php echo $this->input->post('datecheckin'); ?>" readonly <?php echo ($this->input->post('rbtonsite') == 'walkin' ? 'disabled' : ''); ?> />
						<span class="input-group-btn">
							<button class="btn btn-default" type="button" <?php echo ($this->input->post('rbtonsite') == 'walkin' ? 'disabled' : ''); ?> >
								<span class="glyphicon glyphicon-calendar"></span>
							</button>
						</span>
					</div>
				</li>
				<li><label for="datecheckout" class="control-label">Number of days</label></li>
				<li><select name="datecheckout" class="form-control">
					<option value="none" <?php echo set_select('datecheckout', 'none'); ?>>---Pick number---</option>
					<?php
						for ($x = 1; $x <= 31; $x++):
							echo '<option value="' . $x . '"' . set_select('datecheckout', $x) . '>' . $x . '</option>';
						endfor;
					?>
					</select></li>
				<li><label for="paymentmethod" class="control-label">Payment Method</label></li>
				<li><select name="paymentmethod" id="paymentmethod" class="form-control" <?php echo ($this->input->post('rbtonsite') != null ? 'disabled' : ''); ?>>
					<option value="none" <?php echo ($this->input->post('paymentmethod') == 'none' ? 'selected' : ''); ?>>---Pick method---</option>
					<?php
						foreach ($methods as $m):
							echo '<option value="' . $m['method_id'] . '"' . set_select('paymentmethod', $m['method_id']) . ($this->input->post('paymentmethod') == $m['method_id'] ? 'selected' : '') . '>' . $m['method_desc'] . '</option>';
						endforeach;
					?>
				</select></li>
				<br />
				<li><label><input type="radio" name="rbtonsite" id="rbtonsite" value="onsite" <?php echo ($this->input->post('rbtonsite') == 'onsite' ? 'checked' : ''); ?> /> I am paying at the resort.</label></li>
				<li><span class="help-block">Choose this if you prefer to pay at the resort.</span></li>
				<li><label><input type="radio" name="rbtonsite" id="rbtonsite" value="walkin" <?php echo ($this->input->post('rbtonsite') == 'walkin' ? 'checked' : ''); ?> /> I am a walk-in customer.</label></li>
				<li><span class="help-block">Choose this if you are a walk-in customer.</span></li>
				<br />
				<li><p>By clicking Submit, you agree to our <a href="#" class="navbar-link bg-success">Terms and Conditions</a>.</p></li>
				<li><span class="pull-right"><input type="submit" name="btnsubmit" class="btn btn-success" value="Submit Details"
					<?php
					if (count($_SESSION['rsvdata']) == 0) {
						echo 'disabled';
					}
				?> />
				<input type="reset" name="btnclear" class="btn btn-primary" value="Clear Fields" /></span></li>
				</div>
		<?php echo form_close(); ?>
		</ul>
	</nav>

	<div class="col-sm-8">
		<div class="row">
			<div class="col-sm-12">
				<h1>Rooms</h1>
				<nav class="nav navbar-default">
					<ul class="nav">
						<li><p class="navbar-text">Show rooms by room type:</p></li>
						<?php echo form_open('reserve/filterRooms', array('class' => 'navbar-form')); ?>
						<div class="form-group">
							<select id="filtertype" name="filtertype" class="form-control">
								<option value="all">All Rooms</option>
							<?php
								foreach ($rtype as $rt):
									echo '<option value="' . $rt['room_name_desc'] . '">' . $rt['room_name_desc'] . '</option>';
								endforeach;
							?>
							</select>
						</div>
						<input type="submit" name="btnfilter" id="btnfilter" class="btn btn-primary" value="Show" />
						<?php echo form_close(); ?>
					</ul>
				</nav>
