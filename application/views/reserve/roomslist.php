				<table class="table table-condensed table-bordered">
					<tr>
						<td>
							<?php
							foreach ($c as $content):
								echo '<a href="' . base_url() . $content['content'] . '" rel="gallery' . $w . '" class="fancybox"><img src="' . base_url() . $content['content'] . '" width="150" height="150" /></a>';
							endforeach;
							?>
							<p>Click the image above<br />to enlarge.</p>
						</td>
						<td>
							<table>
							<tr>
								<td><strong>Room Name:</strong></td>
								<td><?php echo $room['real_name']; ?></td>
							</tr>
							<tr>
								<td><strong>Room Type:</strong></td>
								<td><?php echo $room['room_name_desc']; ?></td>
							</tr>
							<tr>
								<td><strong>Description:</strong></td>
								<td><?php echo $room['roomdesc']; ?></td>
							</tr>
							<tr>
								<td><strong>Capacity:</strong></td>
								<td><?php echo $room['min_capacity']; ?> - <?php echo $room['max_capacity']; ?> persons</td>
							</tr>
							<tr>
								<td><strong>Remarks:</strong></td>
								<td><?php echo $room['remarks']; ?></td>
							</tr>
							<tr>
								<td><strong>Status:</strong></td>
								<td><?php echo $room['roomstatus']; ?></td>
							</tr>
							<tr>
								<td><strong>Price per day:</strong></td>
								<td><strong>PHP</strong> <?php echo $room['roomrate']; ?></td>
							</tr>
							<?php
								echo form_open('reserve/createReserve/add/' . $room['room_id'], array('id' => $room['room_capacity_desc'], 'class' => 'form-horizontal'));
							?>
							<tr>
								<td>&nbsp;</td>
								<td><input type="submit" name="btnrsv" id="btnrsv<?php echo $room['room_capacity_desc']; ?>" class="btn btn-primary" value="Select this Room" <?php
										if ($room['roomstatus'] != 'Available') {
											echo 'disabled';
										}
									?> /></td>
							</tr>
							<?php echo form_close(); ?>
							</table>
						</td>
					</tr>
				</table>
