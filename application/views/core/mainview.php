<main>
	<?php
		if (isset($_SESSION['alerts'])):
			echo $_SESSION['alerts'];
			unset($_SESSION['alerts']);
		endif;
	?>
	<div class="container pull-right">
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br /><br /><br />
		<h1>A place of beautiful sunsets, laughter, and fun.</h1>
		<p>If you&apos;re looking for a place to bond with friends and family this is the right place to be.
		Its single and laid-back atmosphere will make you feel comfortable with the people you always wanted to be with or 
		bond with people you want to know more about of and who knows, you just might be friends with strangers.</p>
	</div>
</main>
