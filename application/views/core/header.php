<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Marine Beach Resort</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom.css" />
	<script src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script src="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function showServerTime(obj, time) {
 		    var parts   = time.split(":"),
		        newTime = new Date(),
		        timeDifference  = new Date().getTime() - newTime.getTime();
		 
		    newTime.setHours(parseInt(parts[0], 10));
		    newTime.setMinutes(parseInt(parts[1], 10));
		    newTime.setSeconds(parseInt(parts[2], 10));
		 
		    var methods = {
		        displayTime: function () {
		            var now = new Date(new Date().getTime() - timeDifference);
		            obj.text(methods.adjustTime(now.getHours()) + ':' + methods.leadZeros(now.getMinutes(), 2) + ':' + methods.leadZeros(now.getSeconds(), 2) + ' ' + methods.getAmPm(now.getHours()));
		            setTimeout(methods.displayTime, 1000);
		        },
		 
		        leadZeros: function (time, width) {
		            while (String(time).length < width) {
		                time = "0" + time;
		            }
		            return time;
		        },

		        adjustTime: function (h) {
		        	if (h > 12) {
		        		return h % 12;
		        	}
		        	else {
		        		if (h == 0)
		        			return 12;
		        		else
		        			return h;
		        	}
		        },

		        getAmPm: function (h) {
		        	var m = h > 12 ? 'PM' : 'AM';
		        	return m;
		        }
		    };
		    methods.displayTime();
		}

		$(document).ready(function() {
			$('.carousel').carousel({'interval': 4000});
			var serverClock = $("#clock2");
	 		if (serverClock.length > 0) {
			    showServerTime(serverClock, serverClock.text());
			}
		});
	</script>
	<style type="text/css">
		h1, p {
			color: #FFF;
		}
	</style>
</head>

<body>
	<div id="mainCarousel" class="carousel container slide">
		<div class="carousel-inner">
			<div class="item active one"></div>
			<div class="item two"></div>
			<div class="item three"></div>
		</div>
	</div>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a href="<?php echo base_url(); ?>" class="navbar-brand">Marine Beach Resort</a>
		</div>
		<ul class="nav navbar-nav navbar-left">
			<li><a href="<?php echo base_url(); ?>reserve">Reserve</a></li>
			<li><a href="#">Gallery</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#">Description</a></li>
					<li><a href="#">Contact Us</a></li>
					<li><a href="#">Terms and Conditions</a></li>
					<li><a href="<?php echo base_url(); ?>privacy">Privacy Policy</a></li>
				</ul>
			</li>
			<li><a href="#">Help</a></li>
			<li><strong><p id="clock2" class="navbar-text"><?php echo date('g:i:s'); ?></p></strong></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
		<?php echo $cust; ?>
		<li><?php echo $login; ?><li>
		</ul>
	</div>
</nav>
