					<tr>
						<td>
							<?php
							foreach ($c as $content):
								echo '<a href="' . base_url() . $content['content'] . '" rel="gallery' . $w . '" class="fancybox"><img src="' . base_url() . $content['content'] . '" width="150" height="150" /></a>';
							endforeach;
							?>
							<p>Click the image above<br />to enlarge.</p>
						</td>
						<td>
							<table>
							<tr>
								<td><strong>Room Name:</strong></td>
								<td><?php echo $room[0]['real_name']; ?></td>
							</tr>
							<tr>
								<td><strong>Room Type:</strong></td>
								<td><?php echo $room[0]['room_name_desc']; ?></td>
							</tr>
							<tr>
								<td><strong>Description:</strong></td>
								<td><?php echo $room[0]['roomdesc']; ?></td>
							</tr>
							<tr>
								<td><strong>Capacity:</strong></td>
								<td><?php echo $room[0]['min_capacity']; ?> - <?php echo $room[0]['max_capacity']; ?> persons</td>
							</tr>
							<tr>
								<td><strong>Remarks:</strong></td>
								<td><?php echo $room[0]['remarks']; ?></td>
							</tr>
							<tr>
								<td><strong>Status:</strong></td>
								<td><?php echo $room[0]['roomstatus']; ?></td>
							</tr>
							<tr>
								<td><strong>Price per day:</strong></td>
								<td><strong>PHP</strong> <?php echo $room[0]['roomrate']; ?></td>
							</tr>
							</table>
						</td>
					</tr>
