<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/fancybox/source/jquery.fancybox.css?v=2.1.5" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>datepair/lib/bootstrap-datepicker.css" />
<script src="<?php echo base_url(); ?>js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?php echo base_url(); ?>datepair/lib/bootstrap-datepicker.js"></script>
<script>
	$(document).ready(function() {
		var dt = new Date();
		$('a.fancybox').fancybox();
		$('.date').datepicker({
			'format': 'MM d, yyyy',
			'autoclose': true,
			'startDate': dt
		});
	});
</script>
<main>
	<nav class="col-sm-4 well" data-spy="affix" id="side">
		<center><h1>Your Reservations</h1></center>
		<ul class="nav nav-pills nav-stacked">
		<?php
			$w = 1;
			foreach($rsv as $r):
				echo '<li' . ($w == 1 ? ' class="active"' : '') . '><a href="#rsvid' . $r['rsv_id'] . '"><strong><center>' . date('F d, Y', strtotime($r['rsv_date'])) . '</center></strong></a></li>';
				$w++;
			endforeach;
		?>
		</ul>
	</nav>

	<div class="col-sm-offset-4 col-sm-8">
