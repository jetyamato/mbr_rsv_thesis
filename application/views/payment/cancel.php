<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/css/bootstrap.css" />
	<script src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script>
		setTimeout(function() {
			window.location.href="<?php echo base_url(); ?>";
		}, 4000);
	</script>
</head>
<body>
<main>
	<div class="container">
		<h1>Transaction Cancelled</h1>
		<p>Redirecting you to the home page. Please click <a href="<?php echo base_url(); ?>">here</a> if it doesn't redirect.</p>
	</div>
</main>
</body>
</html>