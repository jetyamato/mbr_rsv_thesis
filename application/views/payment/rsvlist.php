		<a id="rsvid<?php echo $details['rsv_id']; ?>"></a>
		<div class="row">
			<div class="col-sm-12">
				<h1><?php echo date('F d, Y', strtotime($details['rsv_date'])); ?></h1>
				<table class="table table-bordered">
					<tr style="background-color:rgb(181,225,236);"><td colspan="2"><strong>Reservation Details</strong></td></tr>
					<tr>
						<td colspan="2">
							<table>
							<tr>
								<td><strong>Number of Adults:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo $details['no_adults']; ?></td>
							</tr>
							<tr>
								<td><strong>Number of Children:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo $details['no_children']; ?></td>
							</tr>
							<tr>
								<td><strong>Swimming Schedule:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo $details['swim_sched']; ?></td>
							</tr>
							<tr>
								<td><strong>Current Season:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo $costs[0]['season']; ?></td>
							</tr>
							<tr>
								<td><strong>Check-in Date:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo date('F d, Y', strtotime($details['arrival'])); ?></td>
							</tr>
							<tr>
								<td><strong>Check-out Date:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo date('F d, Y', strtotime($details['departure'])); ?></td>
							</tr>
							<tr>
								<td><strong>Days to Stay:</strong></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td><?php echo $costs[0]['num_days']; ?></td>
							</tr>
							</table>
						</td>
					</tr>
					<tr style="background-color:rgb(181,225,236);"><td colspan="2"><strong>Selected Rooms</strong></td></tr>
					<?php echo $roomlist; ?>
					<tr style="background-color:rgb(181,225,236);"><td colspan="2"><strong>Cost Breakdown</strong></td></tr>
					<tr>
						<td colspan="2" <?php echo ($details['rsvstatus'] == 'Pending' ? 'class="bg-warning"' : 'class="bg-success"'); ?>>
							<table>
								<tr>
									<td><strong>Entrance Fee per Adult:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo '<strong>PHP</strong> ' . ($costs[0]['season'] == 'Regular Season' ? $costs[0]['adult_regular_rate'] : $costs[0]['adult_seasonal_rate']); ?></td>
								</tr>
								<tr>
									<td><strong>Subtotal:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo '<strong>PHP</strong> ' . $costs[0]['adult_subtotal']; ?></td>
								</tr>
								<tr>
									<td><strong>Entrance Fee per Child:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo '<strong>PHP</strong> ' . ($costs[0]['season'] == 'Regular Season' ? $costs[0]['children_regular_rate'] : $costs[0]['children_seasonal_rate']); ?></td>
								</tr>
								<tr>
									<td><strong>Subtotal:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo '<strong>PHP</strong> ' . $costs[0]['children_subtotal']; ?></td>
								</tr>
								<tr>
									<td><strong>Room Subtotal:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo '<strong>PHP</strong> ' . $costs[0]['room_subtotal']; ?></td>
								</tr>
								<tr>
									<td><strong>GRAND TOTAL:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo '<strong>PHP ' . $costs[0]['grand_total'] . '</strong>'; ?></td>
								</tr>
								<tr><td><br /></td></tr>
								<tr>
									<td><strong>Reservation Status:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><strong><?php echo strtoupper($details['rsvstatus']); ?></strong></td>
								</tr>
								<tr>
									<td><strong>Payment Method:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo $details['pay_method']; ?></td>
								</tr>
								<tr>
									<td><strong>Reservation Code:</strong></td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td><?php echo $details['rsv_code']; ?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tr>
									<?php if($details['pay_method'] != 'On Site'): ?>
									<?php echo form_open('payment/payDown/' . $details['rsv_id'], array('class' => 'form_horizontal')); ?>
									<input type="submit" name="btnpaydown" id="btnpaydown" class="btn btn-primary" value="Pay Downpayment" />&nbsp;
									<?php echo form_close(); ?>
									<?php echo form_open('payment/payFull/' . $details['rsv_id'], array('class' => 'form_horizontal')); ?>
									<input type="submit" name="btnpayfull" id="btnpayfull" class="btn btn-primary" value="Pay Full Total" />&nbsp;
									<?php echo form_close(); ?>
									<?php endif; ?>
									<?php echo form_open('payment/cancelRsv/' . $details['rsv_id'], array('class' => 'form_horizontal')); ?>
									<input type="submit" name="btncancel" id="btncancel" class="btn btn-danger" value="Cancel Reservation" />
									<?php echo form_close(); ?>
								</tr>
								<tr>
									<td>
										<i>You must present this reservation code <?php echo ($details['pay_method'] == 'On Site' ? 'upon payment' : 'upon arrival at the resort'); ?>, along with valid identification for verification.<br />
										At least 30% of your grand total must be paid fully within 3 days after the date of your reservation to confirm the reservation.<br />
										Failure to comply will result in your reservation to be cancelled.</i>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
