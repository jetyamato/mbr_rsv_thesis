<script>
	$(document).ready(function() {
		$('.nav-pills a').click(function(event) {
			$('#right').load("<?php echo base_url(); ?>staff/cms/" + this.hash.replace('#',''));
			$('li').not(this).removeClass('active');
			$(this).parent('li').attr('class', 'active');
			$('#alerts').removeAttr('class');
			$('#alerts').html('');
		});
	});
</script>
<main>
	<div class="container">
	<div data-spy="affix">
		<div id="alerts"></div>
		<h1>Content Management</h1>
		<nav class="well">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="#rooms">Rooms</a></li>
				<li><a href="#news">News</a></li>
				<li><a href="#announcements">Announcements</a></li>
				<li><a href="#gallery">Gallery</a></li>
				<li><a href="#desc">Description Page</a></li>
				<li><a href="#contact">Contact Us Page</a></li>
				<li><a href="#terms">Terms and Conditions Page</a></li>
				<li><a href="#privacy">Privacy Policy Page</a></li>
				<li><a href="#help">Help Page</a></li>
			</ul>
		</nav>
	</div>
	<div id="right" class="col-sm-offset-4">
		<h1>Main Panel</h1>
		<div class="jumbotron">
			<h1>How to Use</h1>
			<p>Select the criteria of the content you want to edit from the left.</p>
		</div>
	</div>
	</div>
</main>
</body>
</html>