<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Marine Beach Resort</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom.css" />
	<script src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script src="<?php echo base_url(); ?>css/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function showServerTime(obj, time) {
 		    var parts   = time.split(":"),
		        newTime = new Date(),
		        timeDifference  = new Date().getTime() - newTime.getTime();
		 
		    newTime.setHours(parseInt(parts[0], 10));
		    newTime.setMinutes(parseInt(parts[1], 10));
		    newTime.setSeconds(parseInt(parts[2], 10));
		 
		    var methods = {
		        displayTime: function () {
		            var now = new Date(new Date().getTime() - timeDifference);
		            obj.text(methods.adjustTime(now.getHours()) + ':' + methods.leadZeros(now.getMinutes(), 2) + ':' + methods.leadZeros(now.getSeconds(), 2) + ' ' + methods.getAmPm(now.getHours()));
		            setTimeout(methods.displayTime, 1000);
		        },
		 
		        leadZeros: function (time, width) {
		            while (String(time).length < width) {
		                time = "0" + time;
		            }
		            return time;
		        },

		        adjustTime: function (h) {
		        	if (h > 12) {
		        		return h % 12;
		        	}
		        	else {
		        		if (h == 0)
		        			return 12;
		        		else
		        			return h;
		        	}
		        },

		        getAmPm: function (h) {
		        	var m = h > 12 ? 'PM' : 'AM';
		        	return m;
		        }
		    };
		    methods.displayTime();
		}

		$(document).ready(function() {
			var serverClock = $("#clock2");
	 		if (serverClock.length > 0) {
			    showServerTime(serverClock, serverClock.text());
			}
		});
	</script>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a href="staff" class="navbar-brand">Staff &amp; Management</a>
		</div>
		<ul class="nav navbar-nav navbar-left">
			<li><strong><p id="clock2" class="navbar-text"><?php echo date('g:i:s'); ?></p></strong></li>
		</ul>
	</div>
</nav>
