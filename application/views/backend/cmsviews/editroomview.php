				<script>
					$(document).ready(function() {
						$('[type=button]').click(function() {
							$('#right').load("<?php echo base_url(); ?>staff/cms/rooms");
							$('#alerts').removeAttr('class');
							$('#alerts').html('');
						})

						$('#details').submit(function(event) {
							event.preventDefault();
							
							$.ajax({
								type: "POST",
								url: $('#details').attr('action'),
								data: new FormData(this),
								processData: false,
								contentType: false,
								success: function(json) {
									if (json.success == false) {
										$('#alerts').attr('class', 'alert alert-danger');
										$('#alerts').html(json.errors);
									}
									else {
										$('#alerts').attr('class', 'alert alert-success');
										$('#alerts').html('Room data updated successfully.');
										$('#right').load("<?php echo base_url(); ?>staff/cms/rooms");
									}
								},
								dataType: 'json'
							});

							/*$.post($('#details').attr('action'), formData, function(json) {
								if (json.success == false) {
									$('#alerts').attr('class', 'alert alert-danger');
									$('#alerts').html(json.errors);
								}
								else {
									$('#alerts').attr('class', 'alert alert-success');
									$('#alerts').html('Room data updated successfully.');
									$('#right').load("<?php #echo base_url(); ?>staff/cms/rooms");
								}
							}, 'json');*/
						});

						$('#roomtype').change(function() {
							$('#roomtypeinfo').load("<?php echo base_url(); ?>staff/cms/getRoomTypeInfo/" + $(this).val());
						});
					});
				</script>
				<h1>Edit Room Details</h1>
				<i>Note: Navigating to other links at the left pane of the Content Management with unsaved changes will result in the loss of unsaved changes.</i>
					<?php echo form_open_multipart('backend/cms/edit/rooms/' . $id, array('id' => 'details', 'class' => 'form-horizontal')); ?>
					<table class="table table-bordered">
						<tr>
							<td colspan="2">
								<button type="submit" name="btnupdate" id="btnupdate" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update Details</button>
								<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
								<button type="button" name="btncancel" id="btncancel" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							</td>
						</tr>
						<tr>
							<td><strong>Current Image for this room:</strong></td>
							<td><img src="<?php echo base_url() . $curimg; ?>" /></td>
						</tr>
						<tr>
							<td><strong>Select image to upload:</strong></td>
							<td><fieldset><input type="file" name="imginput" /></fieldset></td>
						</tr>
						<tr>
							<td><strong>Room Name:</strong></td>
							<td><input type="text" name="roomname" id="roomname" class="form-control" value="<?php echo $room['real_name']; ?>"/></td>
						</tr>
						<tr>
							<td><strong>Room Type:</strong></td>
							<td>
								<select name="roomtype" id="roomtype" class="form-control">
									<?php foreach ($roomtype as $rt): ?>
										<option value="<?php echo $rt['room_name_id']; ?>" <?php echo ($rt['room_name_desc'] == $room['room_name_desc'] ? 'selected' : ''); ?>><?php echo $rt['room_name_desc']; ?></option>
									<?php endforeach; ?>
								</select><br />
								<div id="roomtypeinfo">
								<strong>Minimum Capacity: </strong><?php echo ($room['min_capacity'] > 1 ? $room['min_capacity'] . ' persons' : $room['min_capacity'] . ' person'); ?><br />
								<strong>Maximum Capacity: </strong><?php echo ($room['max_capacity'] > 1 ? $room['max_capacity'] . ' persons' : $room['max_capacity'] . ' person'); ?><br />
								<strong>Price per day: </strong><?php echo $room['roomrate']; ?>
								</div>
							</td>
						</tr>
						<tr>
							<td><strong>Description:</strong></td>
							<td><textarea name="roomdesc" id="roomdesc" class="form-control"><?php echo $room['roomdesc']; ?></textarea></td>
						</tr>
						<tr>
							<td><strong>Remarks:</strong></td>
							<td><input type="text" name="remarks" id="remarks" class="form-control" value="<?php echo $room['remarks']; ?>"/></td>
						</tr>
						<tr>
							<td><strong>Status:</strong></td>
							<td>
								<select name="roomstatus" class="form-control">
									<option value="available" <?php echo ($room['roomstatus'] == 'Available' ? 'selected' : ''); ?>>Available</option>
									<option value="unavailable" <?php echo ($room['roomstatus'] == 'Unavailable' ? 'selected' : ''); ?>>Unavailable</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<button type="submit" name="btnupdate" id="btnupdate" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update Details</button>
								<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
								<button type="button" name="btncancel" id="btncancel" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							</td>
						</tr>
					</table>
					<?php echo form_close(); ?>