				<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/fancybox/source/jquery.fancybox.css?v=2.1.5" />
				<script src="<?php echo base_url(); ?>js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
				<script>
					$(document).ready(function() {
						$('.fancybox').fancybox();

						$('a.btn-primary').click(function(event) {
							event.preventDefault();
							$('#right').load(this.href);
						});

						$('a.btn-danger').click(function(event) {
							event.preventDefault();
							$('#right').load(this.href);
						});
					});
				</script>
				<h1>Rooms</h1>
				<div id="rooms">
					<table class="table table-bordered">
					<?php echo $roomslist; ?>
					</table>
				</div>