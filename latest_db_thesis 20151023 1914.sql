-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.39


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema reservation
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ reservation;
USE reservation;

--
-- Table structure for table `reservation`.`cmsrooms`
--

DROP TABLE IF EXISTS `cmsrooms`;
CREATE TABLE `cmsrooms` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(100) NOT NULL,
  `related_to` varchar(100) NOT NULL,
  `source_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`cmsrooms`
--

/*!40000 ALTER TABLE `cmsrooms` DISABLE KEYS */;
INSERT INTO `cmsrooms` (`cms_id`,`content`,`related_to`,`source_id`) VALUES 
 (1,'pics/new/standard2/standard5.jpg','rooms',1),
 (2,'pics/new/standard2/standard2.jpg','rooms',2),
 (3,'pics/new/standard2/standard3.jpg','rooms',3),
 (4,'pics/new/family/family.jpg','rooms',4),
 (5,'pics/new/standard6/standard.jpg','rooms',5),
 (6,'pics/new/standard6/standard2.jpg','rooms',6),
 (7,'pics/new/standard6/standard3.jpg','rooms',7),
 (8,'pics/new/dorm/dorm.jpg','rooms',8),
 (9,'pics/new/family/family2.jpg','rooms',9);
/*!40000 ALTER TABLE `cmsrooms` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `cust_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(200) NOT NULL DEFAULT '',
  `middlename` varchar(200) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(45) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `cellphone` varchar(20) DEFAULT NULL,
  `cust_status` set('pending','confirmed') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`cust_id`),
  UNIQUE KEY `email` (`email`) USING HASH,
  UNIQUE KEY `username` (`user_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`customers`
--

/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`cust_id`,`surname`,`firstname`,`middlename`,`user_id`,`email`,`address`,`telephone`,`cellphone`,`cust_status`) VALUES 
 (1,'Tamayo','Joseph','',1,'tamayo209@gmail.com','QC','','','confirmed'),
 (2,'Boter','Breinier','',3,'breinierboter@yahoo.com','QC','','','confirmed'),
 (3,'Blas','Mark','',4,'markblas@yahoo.com','QC','','','confirmed'),
 (4,'Estrada','Jeff','N',5,'jeff@yahoo.com','rizal','','','confirmed'),
 (9,'Quinto','Gilmer','',10,'gilmerquinto@yahoo.com','QC','','','confirmed'),
 (10,'Masangya','Pius','',11,'piusvillarde@gmail.com','QC','','','confirmed'),
 (11,'Aaa','Aaa','',12,'aaa@gmail.com','Manila','','','pending');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`entrance_fee`
--

DROP TABLE IF EXISTS `entrance_fee`;
CREATE TABLE `entrance_fee` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `age_type` set('Adult','Children') NOT NULL DEFAULT '',
  `daytime_fee` decimal(9,2) unsigned NOT NULL DEFAULT '0.00',
  `overnight_fee` decimal(9,2) unsigned NOT NULL DEFAULT '0.00',
  `fee_type` set('Regular','Seasonal') NOT NULL DEFAULT '',
  PRIMARY KEY (`fee_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`entrance_fee`
--

/*!40000 ALTER TABLE `entrance_fee` DISABLE KEYS */;
INSERT INTO `entrance_fee` (`fee_id`,`age_type`,`daytime_fee`,`overnight_fee`,`fee_type`) VALUES 
 (1,'Adult','70.00','100.00','Regular'),
 (2,'Children','50.00','70.00','Regular'),
 (3,'Adult','100.00','150.00','Seasonal'),
 (4,'Children','70.00','100.00','Seasonal');
/*!40000 ALTER TABLE `entrance_fee` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`facilities`
--

DROP TABLE IF EXISTS `facilities`;
CREATE TABLE `facilities` (
  `facility_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facility` varchar(45) NOT NULL DEFAULT '',
  `description` set('Billiards Table','Videoke','Single Kayak','Double Kayak','Speedboat','Jet Ski','Life vest/Floaters','Massage Services','3-man Tent','6-man Tent','Picnic Hut/Cottage') NOT NULL,
  `min_reservation_time` time NOT NULL,
  `max_reservation_time` time NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `fac_status` set('Available','Unavailable') NOT NULL,
  PRIMARY KEY (`facility_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`facilities`
--

/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;
INSERT INTO `facilities` (`facility_id`,`facility`,`description`,`min_reservation_time`,`max_reservation_time`,`remarks`,`fac_status`) VALUES 
 (1,'table1','Billiards Table','00:00:00','01:00:00','','Available'),
 (2,'table2','Billiards Table','00:00:00','01:00:00','','Available'),
 (3,'videoke','Videoke','00:00:00','01:00:00','','Available'),
 (4,'single_kayak','Single Kayak','00:00:00','01:00:00','','Available'),
 (5,'double_kayak','Double Kayak','00:00:00','01:00:00','','Available'),
 (6,'speedboat','Speedboat','00:00:00','01:00:00','','Available'),
 (8,'lifevest','Life vest/Floaters','00:00:00','01:00:00','','Available');
/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`facility_price`
--

DROP TABLE IF EXISTS `facility_price`;
CREATE TABLE `facility_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fac_type` set('Billiards Table','Videoke','Single Kayak','Double Kayak','Speedboat','Jet Ski','Life vest/Floaters','Massage Services','3-man Tent','6-man Tent','Picnic Hut/Cottage') NOT NULL,
  `reg_price` decimal(9,2) unsigned NOT NULL DEFAULT '0.00',
  `seasonal_price` decimal(9,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`facility_price`
--

/*!40000 ALTER TABLE `facility_price` DISABLE KEYS */;
INSERT INTO `facility_price` (`price_id`,`fac_type`,`reg_price`,`seasonal_price`) VALUES 
 (1,'Billiards Table','120.00','140.00'),
 (2,'Videoke','350.00','420.00'),
 (3,'Single Kayak','250.00','300.00'),
 (4,'Double Kayak','350.00','420.00'),
 (5,'Speedboat','4500.00','5400.00'),
 (7,'Life vest/Floaters','150.00','180.00');
/*!40000 ALTER TABLE `facility_price` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE `payment_methods` (
  `method_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `method_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`payment_methods`
--

/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` (`method_id`,`method_desc`) VALUES 
 (1,'PayPal'),
 (2,'Credit Card (through PayPal)'),
 (3,'Money Transfer (Cebuana, Palawan Express, Western Union)');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`room_capacity`
--

DROP TABLE IF EXISTS `room_capacity`;
CREATE TABLE `room_capacity` (
  `room_capacity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_capacity_desc` varchar(100) NOT NULL,
  `min_capacity` int(10) unsigned NOT NULL,
  `max_capacity` int(10) unsigned NOT NULL,
  `roomrate` decimal(8,2) NOT NULL,
  PRIMARY KEY (`room_capacity_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`room_capacity`
--

/*!40000 ALTER TABLE `room_capacity` DISABLE KEYS */;
INSERT INTO `room_capacity` (`room_capacity_id`,`room_capacity_desc`,`min_capacity`,`max_capacity`,`roomrate`) VALUES 
 (1,'2persons',1,3,'800.00'),
 (2,'6persons',4,6,'1500.00'),
 (3,'10plus',10,15,'3500.00'),
 (4,'10persons',8,10,'2500.00');
/*!40000 ALTER TABLE `room_capacity` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`room_name`
--

DROP TABLE IF EXISTS `room_name`;
CREATE TABLE `room_name` (
  `room_name_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_name_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`room_name_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`room_name`
--

/*!40000 ALTER TABLE `room_name` DISABLE KEYS */;
INSERT INTO `room_name` (`room_name_id`,`room_name_desc`) VALUES 
 (1,'Standard Room (2 persons)'),
 (2,'Standard Room (6 persons)'),
 (3,'Dormitory'),
 (4,'Family Room');
/*!40000 ALTER TABLE `room_name` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `room_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_capacity_id` int(10) unsigned NOT NULL,
  `room_name_id` int(10) unsigned NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `roomdesc` text NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `roomstatus` set('Available','Unavailable') NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data for table `reservation`.`rooms`
--

/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`room_id`,`room_capacity_id`,`room_name_id`,`real_name`,`roomdesc`,`remarks`,`roomstatus`) VALUES 
 (1,1,1,'Room D','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (2,1,1,'Room E','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (3,1,1,'Room F','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (4,4,4,'Room B','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (5,2,2,'Room C','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (6,2,2,'Room G','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (7,2,2,'Room H','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available'),
 (8,3,3,'Room A','Air-conditioned room with 1 double-deck bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available');
INSERT INTO `rooms` (`room_id`,`room_capacity_id`,`room_name_id`,`real_name`,`roomdesc`,`remarks`,`roomstatus`) VALUES 
 (9,4,4,'Room I','Air-conditioned room with 1 pulling bed, 1 TV unit with cable, 1 cabinet with mirror, terrace, bathroom','Sample','Available');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`rsv_facilities`
--

DROP TABLE IF EXISTS `rsv_facilities`;
CREATE TABLE `rsv_facilities` (
  `rsv_fac_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rsv_id` int(10) unsigned NOT NULL,
  `facility_id` int(10) unsigned NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  PRIMARY KEY (`rsv_fac_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`rsv_facilities`
--

/*!40000 ALTER TABLE `rsv_facilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `rsv_facilities` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`rsv_master`
--

DROP TABLE IF EXISTS `rsv_master`;
CREATE TABLE `rsv_master` (
  `rsv_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cust_id` int(10) unsigned NOT NULL DEFAULT '0',
  `no_adults` int(10) unsigned NOT NULL DEFAULT '0',
  `no_children` int(10) unsigned NOT NULL DEFAULT '0',
  `swim_sched` set('Daytime','Overnight') NOT NULL,
  `arrival` date NOT NULL DEFAULT '0000-00-00',
  `departure` date NOT NULL DEFAULT '0000-00-00',
  `grand_total` decimal(9,2) NOT NULL,
  `rsv_date` date NOT NULL,
  `pay_method` set('On Site','Money Transfer','Online','Credit') NOT NULL DEFAULT '',
  `rsvstatus` set('Pending','Confirmed','Done','Cancelled') NOT NULL DEFAULT 'Pending',
  `rsv_code` bigint(20) unsigned NOT NULL DEFAULT '0',
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`rsv_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`rsv_master`
--

/*!40000 ALTER TABLE `rsv_master` DISABLE KEYS */;
INSERT INTO `rsv_master` (`rsv_id`,`cust_id`,`no_adults`,`no_children`,`swim_sched`,`arrival`,`departure`,`grand_total`,`rsv_date`,`pay_method`,`rsvstatus`,`rsv_code`,`remarks`) VALUES 
 (1,1,9,2,'Daytime','2015-10-28','2015-10-31','10730.00','2015-10-16','Online','Pending',1444962714,NULL),
 (2,1,5,0,'Daytime','2015-10-21','2015-10-24','6350.00','2015-10-16','Credit','Pending',1445014026,NULL);
/*!40000 ALTER TABLE `rsv_master` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`rsv_rooms`
--

DROP TABLE IF EXISTS `rsv_rooms`;
CREATE TABLE `rsv_rooms` (
  `rsv_room_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rsv_id` int(10) unsigned NOT NULL,
  `room_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rsv_room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`rsv_rooms`
--

/*!40000 ALTER TABLE `rsv_rooms` DISABLE KEYS */;
INSERT INTO `rsv_rooms` (`rsv_room_id`,`rsv_id`,`room_id`) VALUES 
 (1,1,4),
 (2,2,7);
/*!40000 ALTER TABLE `rsv_rooms` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rsv_id` int(10) unsigned NOT NULL,
  `paypal_id` varchar(100) DEFAULT NULL,
  `amount_paid` decimal(9,2) NOT NULL,
  `trans_date` datetime NOT NULL,
  `trans_type` set('On Site','Money Transfer','Online') NOT NULL DEFAULT '',
  `remarks` varchar(100) DEFAULT NULL,
  `trans_status` set('Void','Active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`transactions`
--

/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` (`transaction_id`,`rsv_id`,`paypal_id`,`amount_paid`,`trans_date`,`trans_type`,`remarks`,`trans_status`) VALUES 
 (1,1,'PAY-4BX61280M2613230JKYQXG6Q','3219.00','2015-10-17 00:12:35','Online','Downpayment','Active'),
 (3,2,'PAY-7RK72469N9841513XKYQW6KQ','1905.00','2015-10-17 05:41:58','Online',NULL,'Active'),
 (4,1,'PAY-4BX61280M2613230JKYQXG6Q','3219.00','2015-10-17 06:00:22','Online','Downpayment','Active');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;


--
-- Table structure for table `reservation`.`users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(150) NOT NULL,
  `salt` varchar(512) NOT NULL,
  `usertype` set('customer','staff','admin') NOT NULL DEFAULT 'customer',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `salt` (`salt`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`.`users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`,`username`,`password`,`salt`,`usertype`) VALUES 
 (1,'josephtamayo','dbe63ecc1d1aca09c561382739ca6afb51b55c3ea77fa93be3a2d9214a113d4401c12ca71b099ddc8ecf5fc8b6b4a01c78218713a66d58f9dd522c0071e42510','2rlGTBUcQ_4MK8urCzPG2oyLf99skks_yFGxLxpvLJF5sHTeSoOtz99BxxOeCgiUukSm4Rb9kFiMz5ZLH9WNBokDqvqM5Fru-FhbO4lzoYvXXgQmYtBnfSBW1jmrjQl-Mx6j_pGZw1nz_OZT-B4jlHA78vOvZAIG1IjPjyZgFBbUCyiL_5TWqV5nLkllNfn99_EE8CHY2iU63ZTQKku5mTe-W-glSix5Yh3ye5PVLh5LAXCZ2v87uF2AfZlLO4A5QvrCGfr2CHvUOzj1zfSYG3Wxq2LkzN4mw-M_KIlxTpPi_f33lss2kzlMBJC_JVWA0v6gnsXwlX2zXn1m8o1Esv8KQswLPS2qRqjl3aDEJxNa5Mc7GiDT-Gz70uU7c9X70MBWU_LW8zZfp2nacgk4K6rj8_OWZGHH-qabHpw2hRnaAWkP3hlxXf9Vh9fcKS2bu-SO2KchDe_hmiG-xhAA3fQRCDFJRJqxJexQUmjLMCqVTpGadQTSis6dpN60xMid','customer'),
 (2,'emmanueltamayo','c70881606836a0777dc4740286f3517bdb58e6dc77c134023ac2c8aee289171198c4517ccc96ce7842a56562b30401137b71f6d6f6ec16fd8abf5c280b5fb97f','CPhcA1k96puKWXar8WSZva2oBmGufa4vyQ5YNmdwpQjuze6XfXyrsjlWq6-m3Ozrc2s692P0mnqEPG2gmGGdfgANT6Su6EkbCjWQGcL1QdBY8amuqGO7UXlUHXiDljV54FYP6NcKXvVeT70T1pC-zhXixUPMhkKSqSFYboujecVCqVMkHZfjSLwPLDatvic6zb2ErQ_U6JXMR3PXW75zj2kuGK9WPk8-fbRxHDfbr-HB726td44HFzeudHXLvjwSW8istTo_CNnFLSNEH282La7NEhfTcb1WI5e0nsoOfZya6lDgfWY-qRvmKxu-WA2aRNuy8ljQ4BvtPfaTxYmj5T9R93g6A47GzLaXibjmLLrnRyAAaRZ38TxaNwsmPjVV6Z-Tj4oZSaXlSBd2TAY_0UXbox1PN7RRZ2hqmA-DU7U0-4g5suXkHkgaEGq6GiPxCEzXISwhvOYJbO4D385MAvA0H6VfDY0abLtKTA6-67w99FclNDzcvV1uLaMX8NCV','staff'),
 (3,'breinierboter','06d5d6082e78c4346ea2b4f8503d0bfc47318e2a8b05b1e2bd59ae179e5fbfeea98d0d4543352facd887e232100a27992a161e2dd6d3d2691e209c8e49a57379','_8vyAX38NxUYGkyr-OwH-wYZzYZHXKHwq5l2T226tnsf9s24BL8PzVGI6iMl7ncMaaLwCi77c5iuAkr1fwCtkrQWdh31qZu0fEiAveaUPWL-VbAOLPfgZbvMkhIVfb88BGqPkgpWz67ES1mBeTpHkChaEYSKL19RXPE_tFk54h5lbj7BVJdjWu-zBnNAE57dMIjHw5BF6J5RBeeA1s0z9zbVZ4uu5gQyALai5u2jLIZiHJ6bMmTkh7he9aQXq-est9Jb3q13wkDIuie3XGVfV1T9iP4TD6xhdpKLdF3UhoGE5rMt60Qae4Y60flcwQgFWOMIrykgRaWqE50Y0o5duAVNrGXn_cDrJi_KuLf3szAHAMPele5-qY5YFr3mKfQavI29vcAUQDLKM-7waQw_PGK4FX3Rs5ZMsxXrj0_dhYWOIDNg-WrIbWcYk8KKrIPm_aIOYvLMMYv1WrZZeUkUDuFQxa5srOQXB-CsJgv_JdO799mbHsBZzJZffkhuSk-Q','customer');
INSERT INTO `users` (`user_id`,`username`,`password`,`salt`,`usertype`) VALUES 
 (4,'markblas','f8a1807ff05c8c0c84b6391e829995f75812f61820bc18e986de23158f27f1990e6e29151c6a6d37eb58066038d683472c2a8bc51b3191407ad8cab4ff96ceb4','olW7ExRIMn8e-xPnW-42gpl2osM0z7_pUvZ0YjW840iQmqIKEUBSw9YZuxne1RWZVcPrv63DRRSr0_DFZuLPqw6nq0aAqUtM9aoFF6LFgpE1HzCaAsrMQZ_D687b8HyhcAtU5tXXuB6Cz1Z0XAA22DDGm-SS8bvJTzDpx2PUHRYTifSQMyaGeRsFvRPTJ7vrNDVGWTo0OpNQD16M7iFcXzsvkaTHpwBxdgBjJogsEMVDc_72cUBSkIjIgTV2B0Q-lgJPNlNh-fFXxpMoxARLq7Mrgxr2Okk6ySHiuWmA0e0wKIFBHBdCnrw2faNgqhIqerbZbaG09_W0AxxoAUDREmJ33f5T87pKHdIKo_URflKIJ6eT7yHUnCpjkDGswYB6R7MJzxkt5xlQD-Atd19iwJZsZuUAlaNRh9noSulROchXJytPuEnMa0EonpmA-SIFRhnk04LRven1zTZO0CYKMuVa4Jn9gq56I-_k_Dxxkf1RozeFTkbeqdvwK0gOpggi','customer'),
 (5,'jnestrada','e148b029593f5f68491625417bb1f843f2661d60edbaf0313e0da3dcc7557ca61b267fdd1c014f777477af3c986b810cbd2a54c186a83778971a1ef7cbe3f7b0','O9tSnxZzUFNDZ_Bo1--5U3NbSuq0qYIBRlBOSfpZuI_ZXGxWTKnGKHjWOQ8EbWxe18iPEyVOVl_ukowLJ_t12kDyVnTCWLjttkweiGEKxVPOD8esI2nlNE9jYRPS8EkZewXVqKAJ6FMzTzfKA2RmlKDR5_T_BSbKnktA9YA3PBRgztN9maLftm_zGf35irbHqANl2Fqi8G9gFfJyOgBPOEOf2-WXhc_HRj2D42CBsTFZV2-tGmiRKgz4oL9NiV01u7OuoqDN9ulEbtBBsVkicdT4kWUFCxf94HwV4eCUSpiESeG7CuRssGQnchLOSI_nrsgxDzW26CIeQCKiemRWQrxl6XTIHZYtaSt2wRJsAJyy5kR-tu-fhWtkfIRHHvi-elxC7ZsVOXXgpaVjxjcjYrNIBlvfypiEAvZWJodoYEQGknGcoIkINNYpTNvRvWteUlHWvhGxOETT5Iz4GUQtGGzNL2y1KPmppKvRyFP13vSXwpi8oHvljxd7VOMwOhMk','customer'),
 (10,'gilmerquinto','2f6a54409976fc5aa6b3d8278ca125a3cb94587df6b31b549187a94b582342fdf309ab180934032459095ab98eddad656f55069af7753cd87b2cc30b6e0d6fd7','7eN-MTBW-K6JlH25BGAkBa6Bj-W6MdNZPpyFCrHwIaML9GqC3dfmZ9LgKIZVffZcinnOVhd-zhuwmwAvoaXWRRwcJyyDj10SFIRHf65DMpVB1MRZLt1W5tdRRyGPQBdGNLeX2XkvoeUxXeBsxmCN2eaTr2zbZ7g46qVSLxKojockQzBh7gZotZJoDJ-oTVBaBfx8Lqv8e-gZZPym7n1ihdJU-GSAbjLbg3wa6O0OxZpIYPlERBnilQQJG-68BABNRB7M-LAmh-BdYpT87t-unxy57B3auf685F3aXKCuct-SAvV2it1sXvNaHDAE0kqCpGW686Dht2WndBxARUFoT75GNY9D-In-5G9NogQXrQoR4h5AvjeWeg6CvwytxpXNp7PteWdQehDPZ8gk5aj-4kYqnifU0EJHrZqaAwqVz_aPNwdARZQNICWuJx1S0_IzR7fWGtG5psXHM6xYw3G-FQy6du99TEDoLOaYPmJ0DvbYfILk7DlEV_ysBMKj-cwx','customer');
INSERT INTO `users` (`user_id`,`username`,`password`,`salt`,`usertype`) VALUES 
 (11,'piusmasangya','bc7e6085471f0dbcf453c7c475c8d34a55745e51dff6e684cf082dc9504a2809f73ad585947d3cd35c7417174269455b282621f22fe8a077c53fe297d734535a','-vg8oINul7bOaQx_yHSW5OrpdNTAfdssIUUIbDD-0ik2Rvm1QCloP_fcybUU3JzTbEdibpFAdJt8QOWFOhbW4vg39t7V-5UOWpVsFuSglEsu5ohQfNfXJh-cf783bjQUMzHX3gpGjVawOyFeNe50HgpKgHySt-xzGzeICsFtybAP2I0UVSgBAKZZhHXoF5BqFFipEtS5_aqP7jIZVjJpI94zJrzn0rUuSL7RnvDGp_dke6YslQn_4Ao4M4Ltj7UvQtYKZmNrS-ksy7IWJXBsBSKZccVTMj5YtifXS5AdU-SgwvONgBsrc-UizNAkhXLVvQ7475xfi7xr7YOpNhwRIdTBziKfXDbfZjhqzCAeClBXf_lwNkEdEI-vINXMaKmbdSOFcfNr9MEvPR4hzzLywaPxU7P6U7ViD1zAkE1jn6G24cG0msGseez5MIyOVCUxMoIbJSv_RdFPf3LafTINGL6tToB0Q8oboAhe6YYVAqZWU_sl8fSWt9b_ZiWlZdG2','customer'),
 (12,'aaaaaaa','6332536e75a6393010938a8db3c06d1be87a6a0e49f825bfe2d68b9f9b532d39fe885088b2aa04b55fbbb57cf2b957ef81470ba1ba77ddc8b25a03141ca8c3d5','gtZnlxIyA5RrGXzQhtQZ3e60ssQQpH7KoldW5-Ly-Rfw02gbYa0xFSZfMpwWZx5tv11O2fcZylfNzs5q-2XvXmuasDK0XubNzAo_Pz_rfLatlBetqjEamT00cEYrvsvjVWHgG_4StWYmoiuzSaEpLvmClQWzqvbHzb-egp_QYTS3eBu16f38AAIa6p3qktEAN0xXd9KCdo2qA1DemJ3uPbNCQ0tkDeCkLjMR0m3lPzUWSslDCYdHGaTCqqx7soQqwyFRRTbJ_dxYYORo2vHUsWAaTwGmtRWRRI05RNkpb0W3gCqoQ-u2e5lE7GuFnmOX101O37-0teybqiZzL9bONNwwkeCAp2sQGgm5eek1wn6ksWUUzTAE8Yk95IyjZR5UXVO22EUm7yS-0u4xDuMsytodecer8gROeCp9RbIfhaO6WXmDgJDoiKIdzmraG1jXiDwLYF0snGqFTPshmoPFuqdroBpGRi-UyDDsY_8iromFwxkQQTLCxXNT2l3PAD5U','customer'),
 (13,'admin','2808687cc657de01eac6add370c34e5405c19475212a6455659d2371689b83d823f6ee0fcdaf9c60e416908dc4bf14d35b843af60d6de0f59c4ded4860ccf67f','QLm7XtSKV3lw_ZpUzX5c8QEwOxCrxsEzzY6GDJXoaLu3FEbJ1QrEPGaPpS01BeEct2AooH4Z4C3TA5ZxHfJyGHeFjy-3MZZLj4KJebWFC-lurzSvwd5xF3vd1WWIm_L4qxzGt-NX0xzsCvBEq4IPgucBBkgCKa-Soxn87dAdbFA_iB2JOI648azEAzxLSh0XQ9nbqKNgpKiX5rE5sOXmOatK_iB2FfaPqH7yHRZE1wZvePc5ubYV8dpudIbXaj1pOt_1gpblX5qDMREY74J3ni8zV-AgzdOUyoYik-G0lY5_Kj1DFtg9LT3863Wwb1S4LdYVG6YcgrCMlO7eMqmn-9l45hn-jSvel6Oop_lU9MB9sVQN2Q73FkHYOQerUxhkUaJ3tJryk_89ClHHbE8lMQhHq7qkYvZEv9-BMBwJ7IwImuI0Z4alf7fJzVKvlokO3JriDwl584RiLn8BwgQoM6rwEluJ_97l1Rf61c3zy1UjApYRp5gtHtbGA3Gm6AjX','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


--
-- View structure for view `reservation`.`fac_master`
--

DROP VIEW IF EXISTS `fac_master`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fac_master` AS select `facilities`.`facility_id` AS `facility_id`,`facilities`.`facility` AS `facility`,`facilities`.`description` AS `description`,`facility_price`.`reg_price` AS `reg_price`,`facility_price`.`seasonal_price` AS `seasonal_price`,`facilities`.`min_reservation_time` AS `min_reservation_time`,`facilities`.`max_reservation_time` AS `max_reservation_time`,`facilities`.`remarks` AS `remarks`,`facilities`.`fac_status` AS `fac_status` from (`facilities` left join `facility_price` on((`facilities`.`description` = `facility_price`.`fac_type`)));


--
-- View structure for view `reservation`.`room_master`
--

DROP VIEW IF EXISTS `room_master`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `room_master` AS select `rooms`.`room_id` AS `room_id`,`room_capacity`.`room_capacity_desc` AS `room_capacity_desc`,`room_name`.`room_name_desc` AS `room_name_desc`,`rooms`.`real_name` AS `real_name`,`rooms`.`roomdesc` AS `roomdesc`,`rooms`.`remarks` AS `remarks`,`rooms`.`roomstatus` AS `roomstatus`,`room_capacity`.`min_capacity` AS `min_capacity`,`room_capacity`.`max_capacity` AS `max_capacity`,`room_capacity`.`roomrate` AS `roomrate` from ((`rooms` left join `room_capacity` on((`rooms`.`room_capacity_id` = `room_capacity`.`room_capacity_id`))) left join `room_name` on((`rooms`.`room_name_id` = `room_name`.`room_name_id`)));

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
